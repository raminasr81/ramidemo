As most of my work to date is corporate IP that I can't share, here is a small and fun sample of my work that I can. 

In February of 2015, some colleagues of mine formed a cross-disciplinary team to enter a small engineering competition. There were 2 Mechanical Engineers, 2 Electrical Engineers, 1 Embedded Engineer (myself), and 1 Computer Sceintist (to write PC Software). We called ourselves Team Smartillery. 

All teams in the competition were presented with a fixed bill of materials; a box of KNEX building blocks, 3 pairs of different types of servo motors (180 degree, continuous rotation, and gear), some rubber bands and string, two power supplies and basic electrical components, an STM8 (8-bit microcontroller by ST) development board, and a bluetooth module. 

The goal was to form a cross-disciplinary engineering team to develop - in 10 days! - a robot ping pong ball firing machine with auto-reload made of KNEX, actuated by the servo motors, and controlled by the STM8 micro which in turn was speaking wirelessly through the bluetooth module to a PC or mobile application that had a fire control UI to aim and fire the balls. The competition involved head to head face offs of the robots, speed firing, accuracy, and trick shot components. 

Once the mechanical and electrical designs reached far enough along that I could start writing code, there were only five days left before D-Day, three of which were work days! As you can imagine, I ended up writing a lot of the firmware late at night. Doxygen markup, file headers, function headers and comments are much much lighter than they typically would be in my code as I was in a hurry and didn't think I would ever be sharing the code, but I more or less stuck to my guns when it came to solid architecture, division and encapsulation, code formatting and clarity. 

This zip file includes:
- The embedded source code for the STM8. All source files and header files were written by me (being the only embedded engineer on the team) except for files starting with "stm8", the discover.h header, and the EEPROM module...; those all came with the STM8 discovery board, or with the IAR IDE. I also devised the serial protocol that was used over bluetooth to receive commands from the PC. 
- Some photos of our contraption, and a video. 

The main loop (main.c) initializes hardware (GPIO, timers, and an ADC), application layer software, and then calls a quick and dirty homegrown scheduler that calls four main tasks. One task drives the two LEDs on the STM8 board, the second runs the ADC conversions that tell me the pitch of the gun barrel, and there are tasks for receiving and transmitting Bluetooth packets. 

The USART interrupts (in serial_1_driver.c) drive RX of bytes from the bluetooth module, enabling the receive task when a complete packet is received (to avoid processing that packet in an interrupt). The receive task (in serial_2_bluetooth.c) handles low level functionality like setting the Bluetooth device name or PIN, and passes high level packets up to an application layer command handler (in serial_3_commands.c). This high level command handler decodes the command, calling a jump table per the command code. The different command code handlers actuate the pitch (pitch.c), yaw (yaw.c), and fire control (fire_control.c) modules (the fire control module is in charge of our cocking, slacking and firing mechanisms that fires the loaded round and reloads). 

The pitch mechanism compares the current target angle for pitch, and the ADC measurements (adc.c) in a simple feedback control loop. 

At different points in time, telemetry (back to the PC) is generated which makes its way through the command code module (serial_3_commands.c) to the bluetooth module (serial_2_bluetooth.c) and into a buffered TX packet, which makes its way down to the USART TX interrupt (serial_1_driver.c) and gets transmitted out. 

Finally, there is a low level timer module (timer.c) which implements software timers as well as the PWM signals out to the servo motors. 

Our machine ended up in third place (out of nine), mostly due to our slow rate of fire (which stemmed from our mechanical design choices). Our hit ratio was the best as our aiming mechanism was by far the most accurate, but it took us 10 seconds to fire each shot while others were spitting out one or two rounds a second! We did however win the awards for accuracy/repeatability, aesthetics and UI design. 

I am happy to report that the firmware performed flawlessly on competition day; zero issues. 

Rami Nasr