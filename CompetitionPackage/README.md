﻿Tools INFO:
Link to download IAR Embedded Workbench for STM8:
http://supp.iar.com/Download/SW/?item=EWSTM8-EVAL
The eval license should give you 30 days of development with as much codespace as you want.
Docs INFO:
The docs should all be in the zip file attached below.  For some reason, the downloaded copy of the servo document was corrupted.  Here is a direct link:
https://www.princeton.edu/~mae412/TE...02/292-302.pdf
