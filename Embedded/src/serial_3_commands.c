#include "stm8l15x.h"
#include "stdtypes.h"
#include "serial_1_driver.h"
#include "serial_2_bluetooth.h"
#include "serial_3_commands.h"
#include "scheduler.h"
#include "utility.h"
#include "yaw.h"
#include "pitch.h"
#include "fire_control.h"
#include "adc.h"
#include "timer.h"

static BOOLEAN commandSetYaw           (UINT8 * commandBuffer, UINT8 commandSize);
static BOOLEAN commandSetPitch         (UINT8 * commandBuffer, UINT8 commandSize);
static BOOLEAN commandCockLatchAndSlack(UINT8 * commandBuffer, UINT8 commandSize);
static BOOLEAN commandFire             (UINT8 * commandBuffer, UINT8 commandSize);
static BOOLEAN commandGetCodeRevision  (UINT8 * commandBuffer, UINT8 commandSize);
static BOOLEAN commandDefaultAndCenter (UINT8 * commandBuffer, UINT8 commandSize);
static BOOLEAN commandQueryReadyState  (UINT8 * commandBuffer, UINT8 commandSize);
static BOOLEAN commandQueryAdcVoltages (UINT8 * commandBuffer, UINT8 commandSize);
static BOOLEAN commandSetTim2Prescaler (UINT8 * commandBuffer, UINT8 commandSize);
static BOOLEAN commandSetTim2Modulus   (UINT8 * commandBuffer, UINT8 commandSize);
static BOOLEAN commandSetTim3Prescaler (UINT8 * commandBuffer, UINT8 commandSize);
static BOOLEAN commandSetTim3Modulus   (UINT8 * commandBuffer, UINT8 commandSize);

static BOOLEAN commandUnknown          (UINT8 * commandBuffer, UINT8 commandSize);

#define MAX_CAPITAL_LETTER_COMMANDS 26 //One for each letter of the alphabet (in CAPS)

static BOOLEAN (*const commandList[MAX_CAPITAL_LETTER_COMMANDS])(UINT8 *, UINT8) =
{
   commandUnknown,           //'A' (unused)
   commandUnknown,           //'B' (unused)
   commandCockLatchAndSlack, //'C' (Cock, latch and slack)
   commandDefaultAndCenter,  //'D' (Default timer settings and try to center barrel)
   commandUnknown,           //'E' (unused)
   commandFire,              //'F' (Fire!! aka release the latch)
   commandSetTim2Prescaler,  //'G' (unused)
   commandSetTim2Modulus,    //'H' (unused)
   commandSetTim3Prescaler,  //'I' (unused)
   commandSetTim3Modulus,    //'J' (unused)
   commandUnknown,           //'K' (unused)
   commandUnknown,           //'L' (unused)
   commandUnknown,           //'M' (unused)
   commandUnknown,           //'N' (unused)
   commandUnknown,           //'O' (unused)
   commandSetPitch,          //'P' (Pitch. Set barrel pitch to absolute angle)
   commandQueryReadyState,   //'Q' (unused)
   commandGetCodeRevision,   //'R' (Query Code Revision)
   commandUnknown,           //'S' (unused) *RESERVED DUE TO CONFLICT WITH #SMARTILLERY*
   commandUnknown,           //'T' (unused)
   commandUnknown,           //'U' (unused)
   commandQueryAdcVoltages,  //'V' (unused)
   commandUnknown,           //'W' (unused)
   commandUnknown,           //'X' (unused)
   commandSetYaw,            //'Y' (Yaw)
   commandUnknown            //'Z' (unused)
};

#define SMARTILLERY_STRING_SIZE  13
static const UINT8 SmartilleryString[SMARTILLERY_STRING_SIZE] =
{
  DEFAULT_COMMAND_START_CHARACTER,
  'S', 'M', 'A', 'R', 'T', 'I', 'L', 'L', 'E', 'R', 'Y',
  DEFAULT_TERMINATION_CHARACTER
};

#define UNKNOWN_COMMAND_RESPONSE_STRING_SIZE  17
static const UINT8 UnknownCommandResponseString[UNKNOWN_COMMAND_RESPONSE_STRING_SIZE] =
{
   DEFAULT_RESPONSE_START_CHARACTER,
   'U', 'N', 'K', 'N', 'O', 'W', 'N', '_', 'C', 'O', 'M', 'M', 'A', 'N', 'D',
   DEFAULT_TERMINATION_CHARACTER
};

static BOOLEAN extractEightBitNumber(UINT8 * buffer, UINT8* numberPnt);
static BOOLEAN extractSixteenBitNumber(UINT8 * buffer, UINT16* numberPnt);
static BOOLEAN asciiToHexNibble(UINT8 asciiChar, UINT8 * nibble);
static void    pushEightBitNumber(UINT8 * txBuffer, UINT8 numberToPush);
static void    pushSixteenBitNumber(UINT8 * txBuffer, UINT16 numberToPush);

void SerialCommands_HandleReceivedCommand(UINT8 * commandBuffer, UINT8 commandSize)
{
   UINT8 * responseCommand;
   BOOLEAN retVal;
   UINT8   commandChar;

   if ( (commandSize == SMARTILLERY_STRING_SIZE)                                     &&
        (MemCompareEqual(commandBuffer, SmartilleryString, SMARTILLERY_STRING_SIZE))    )
   {
      commandBuffer[0] = DEFAULT_RESPONSE_START_CHARACTER; //Switch '#' to '~' for response
      responseCommand = SerialBluetooth_RequestOutgoingPacketBuffer();
      if (responseCommand != NULL)
      {
         MemCopy(responseCommand, commandBuffer, SMARTILLERY_STRING_SIZE);
         SerialBluetooth_MarkLastRequestedBufferForTransmit(SMARTILLERY_STRING_SIZE);
      }
   }
   else
   {
      commandChar = commandBuffer[1];
      if ( (commandChar >= 'A') && (commandChar <= 'Z') )
      {
         retVal = commandList[commandChar - 'A'](commandBuffer, commandSize);
      }
      else
      {
         retVal = commandUnknown(commandBuffer, commandSize);
      }

      if (!retVal)
      {  //We didnt understand the command. Respond with unknown command response string.
         DEBUGGER_BREAKPOINT();
         responseCommand = SerialBluetooth_RequestOutgoingPacketBuffer();
         if (responseCommand != NULL)
         {
            MemCopy(responseCommand, UnknownCommandResponseString, UNKNOWN_COMMAND_RESPONSE_STRING_SIZE);
            SerialBluetooth_MarkLastRequestedBufferForTransmit(UNKNOWN_COMMAND_RESPONSE_STRING_SIZE);
         }
      }
   }
}

void SerialCommands_TransmitYawResponse(UINT16 angle, UINT8 yawState)
{
   UINT8 * responseCommand;

   responseCommand = SerialBluetooth_RequestOutgoingPacketBuffer();
   if (responseCommand != NULL)
   {
      responseCommand[0]  = DEFAULT_RESPONSE_START_CHARACTER;
      responseCommand[1]  = 'Y';
      responseCommand[2]  = ',';
      pushSixteenBitNumber( &(responseCommand[3]), angle);
      responseCommand[7]  = ',';
      pushEightBitNumber( &(responseCommand[8]), yawState);
      responseCommand[10] = DEFAULT_TERMINATION_CHARACTER;

      SerialBluetooth_MarkLastRequestedBufferForTransmit(11);
   }
}

void SerialCommands_TransmitPitchResponse(UINT16 angle, UINT8 pitchState)
{
   UINT8 * responseCommand;

   responseCommand = SerialBluetooth_RequestOutgoingPacketBuffer();
   if (responseCommand != NULL)
   {
      responseCommand[0]  = DEFAULT_RESPONSE_START_CHARACTER;
      responseCommand[1]  = 'P';
      responseCommand[2]  = ',';
      pushSixteenBitNumber( &(responseCommand[3]), angle);
      responseCommand[7]  = ',';
      pushEightBitNumber( &(responseCommand[8]), pitchState);
      responseCommand[10] = DEFAULT_TERMINATION_CHARACTER;

      SerialBluetooth_MarkLastRequestedBufferForTransmit(11);
   }
}

void SerialCommands_TransmitCockResponse(UINT8 cockState)
{
   UINT8 * responseCommand;

   responseCommand = SerialBluetooth_RequestOutgoingPacketBuffer();
   if (responseCommand != NULL)
   {
      responseCommand[0]  = DEFAULT_RESPONSE_START_CHARACTER;
      responseCommand[1]  = 'C';
      responseCommand[2]  = ',';
      pushEightBitNumber( &(responseCommand[3]), cockState);
      responseCommand[5] = DEFAULT_TERMINATION_CHARACTER;

      SerialBluetooth_MarkLastRequestedBufferForTransmit(6);
   }
}

void SerialCommands_TransmitFireResponse(UINT8 fireState)
{
   UINT8 * responseCommand;

   responseCommand = SerialBluetooth_RequestOutgoingPacketBuffer();
   if (responseCommand != NULL)
   {
      responseCommand[0]  = DEFAULT_RESPONSE_START_CHARACTER;
      responseCommand[1]  = 'F';
      responseCommand[2]  = ',';
      pushEightBitNumber( &(responseCommand[3]), fireState);
      responseCommand[5] = DEFAULT_TERMINATION_CHARACTER;

      SerialBluetooth_MarkLastRequestedBufferForTransmit(6);
   }
}

/*
COMMAND Y, SET YAW TO ANY ANGLE USING 180deg SERVO
 #Y,<AAAA>,<DDDD>,<NNNN>\n
     - <AAAA> : INT16 YawAngle; hexadecimal fixed width (with no "0x"). Value between -90 and 90. This
                is provided merely for the STM8 to store the angle. Its not used in driving the motor.
     - <DDDD> : UINT16 TimerTicksForSignalHigh; hexadecimal fixed width (with no "0x"). Specifies the
                duty cycle by specificying the number of ticks of TIM2 per TIM2 period during which the
                signal shall be high.
     - <NNNN> : UINT16 NumPulses. Specifies the number of pulses to actuate the motor with. */
static BOOLEAN commandSetYaw(UINT8 * commandBuffer, UINT8 commandSize)
{
   BOOLEAN goodToRespond = FALSE;
   UINT16 angle, dutyCycle, numPulses;
   UINT8 yawState;

   if ( (commandBuffer[0] == DEFAULT_COMMAND_START_CHARACTER) &&
        (commandSize >= 17)                                   &&
        (commandBuffer[ 2] == ',')                            &&
        (commandBuffer[ 7] == ',')                            &&
        (commandBuffer[12] == ',')                               )
   {
      if ( !(extractSixteenBitNumber(&commandBuffer[3], &angle)) )
      {
         return FALSE;
      }
      if ( !(extractSixteenBitNumber(&commandBuffer[8], &dutyCycle)) )
      {
         return FALSE;
      }
      if ( !(extractSixteenBitNumber(&commandBuffer[13], &numPulses)) )
      {
         return FALSE;
      }

      Yaw_ExecuteMove( (INT16)angle, dutyCycle, numPulses);

      yawState = 1; //1 means starting to execute the move

      goodToRespond = TRUE;
   }
   else if (commandBuffer[0] == DEFAULT_QUERY_START_CHARACTER)
   {
      angle    = (UINT16) Yaw_GetYawAngle();
      yawState = (UINT8)  Yaw_GetYawState();

      goodToRespond = TRUE;
   }

   if (goodToRespond == TRUE)
   {
      SerialCommands_TransmitYawResponse( (UINT16)angle, yawState);
      return TRUE;
   }
   else
   {
      return FALSE; //No commas where expected or something else wrong in input.. => can't trust this packet!
   }
}

 /*#P,<AAAA>,<DDDD>,<VVVV>,<NNNN>\n
     - <AAAA> : INT16 PitchAngle; hexadecimal fixed width (with no "0x"). Value between -39 and 48. This
                is provided merely for the STM8 to store the angle. Its not used in driving the motor.
     - <DDDD> : UINT16 TimerTicksForSignalHigh; hexadecimal fixed width (with no "0x"). Specifies the
                duty cycle by specificying the number of ticks of TIM3 per TIM3 period during which the
                signal shall be high.
     - <VVVV> : UINT16 ADC_Voltage. Specifies the ADC voltage to seek. 0xFFFF means 'ignore this param'. The
                ADC varies linearly from 1.46V (48deg) to 3.21V (-39deg). This corresponds
                to between 1.46/3.3 * 4095 = 1812 (for 48deg) and 3.21/3.3 * 4095 = 3983 (for -39deg).
     - <NNNN> : UINT16 NumPulses. Specifies the number of pulses to actuate the motor with. ONLY CONSIDERED
                IF <VVVV> != 0xFFFF. This is an optional fallback in case the ADC feedback gets broken somehow.
                0xFFFF means 'ignore this param'.*/
static BOOLEAN commandSetPitch(UINT8 * commandBuffer, UINT8 commandSize)
{
   BOOLEAN goodToRespond = FALSE;
   UINT16 angle, dutyCycle, adcVoltage, numPulses;
   UINT8 pitchState;

   if ( (commandBuffer[0] == DEFAULT_COMMAND_START_CHARACTER) &&
        (commandSize >= 22)                                   &&
        (commandBuffer[ 2] == ',')                            &&
        (commandBuffer[ 7] == ',')                            &&
        (commandBuffer[12] == ',')                            &&
        (commandBuffer[17] == ',')                               )
   {
      if ( !(extractSixteenBitNumber(&commandBuffer[3], &angle)) )
      {
         return FALSE;
      }
      if ( !(extractSixteenBitNumber(&commandBuffer[8], &dutyCycle)) )
      {
         return FALSE;
      }
      if ( !(extractSixteenBitNumber(&commandBuffer[13], &adcVoltage)) )
      {
         return FALSE;
      }
      if ( !(extractSixteenBitNumber(&commandBuffer[18], &numPulses)) )
      {
         return FALSE;
      }

      Pitch_ExecuteMove( (INT16)angle, dutyCycle, adcVoltage, numPulses);

      pitchState = 1; //1 means starting to execute the move

      goodToRespond = TRUE;
   }
   else if (commandBuffer[0] == DEFAULT_QUERY_START_CHARACTER)
   {
      angle      = (UINT16) Pitch_GetPitchAngle();
      pitchState =  (UINT8) Pitch_GetPitchState();

      goodToRespond = TRUE;
   }

   if (goodToRespond == TRUE)
   {
      SerialCommands_TransmitPitchResponse( (UINT16)angle, pitchState);
      return TRUE;
   }
   else
   {
      return FALSE; //No commas where expected or something else wrong in input.. => can't trust this packet!
   }
}

/*COMMAND C, COCK AND SLACK  (USING A CONTINUOUS SERVO)
 #C,<DDDD>,<VVVV>,<EEEE>,<NNNN>\n
     - <DDDD> : UINT16 TimerTicksForSignalHighWhenCocking; hexadecimal fixed width (with no "0x"). Specifies the
                duty cycle by specificying the number of ticks of TIM2 per TIM2 period during which the
                signal shall be high when *cocking* the gun.
     - <VVVV> : UINT16 ADC_Voltage. Specifies the ADC voltage value to use as a stop when cocking.
     - <EEEE> : UINT16 TimerTicksForSignalHighWhenSlacking; hexadecimal fixed width (with no "0x"). Specifies the
                duty cycle by specificying the number of ticks of TIM2 per TIM2 period during which the
                signal shall be high when *slacking* the gun.
     - <NNNN> : UINT16 NumPulses. Specifies the number of pulses to actuate the motor with when slacking.*/
static BOOLEAN commandCockLatchAndSlack(UINT8 * commandBuffer, UINT8 commandSize)
{
   BOOLEAN goodToRespond = FALSE;
   UINT16 dutyCycleForCocking, adcVoltageForCockStop, dutyCycleForSlacking, numPulsesForSlackStop;
   UINT8 cockState;

   if ( (commandBuffer[0] == DEFAULT_COMMAND_START_CHARACTER) &&
        (commandSize >= 22)                                   &&
        (commandBuffer[ 2] == ',')                            &&
        (commandBuffer[ 7] == ',')                            &&
        (commandBuffer[12] == ',')                            &&
        (commandBuffer[17] == ',')                               )
   {
      if ( !(extractSixteenBitNumber(&commandBuffer[3], &dutyCycleForCocking)) )
      {
         return FALSE;
      }
      if ( !(extractSixteenBitNumber(&commandBuffer[8], &adcVoltageForCockStop)) )
      {
         return FALSE;
      }
      if ( !(extractSixteenBitNumber(&commandBuffer[13], &dutyCycleForSlacking)) )
      {
         return FALSE;
      }
      if ( !(extractSixteenBitNumber(&commandBuffer[18], &numPulsesForSlackStop)) )
      {
         return FALSE;
      }

      FireControl_CockLatchAndSlack( dutyCycleForCocking   ,
                                     adcVoltageForCockStop ,
                                     dutyCycleForSlacking  ,
                                     numPulsesForSlackStop   );

      return TRUE; //The Fire Control module will decide what cock state to feed back
   }
   else if (commandBuffer[0] == DEFAULT_QUERY_START_CHARACTER)
   {
      cockState = FireControl_GetCockState();

      goodToRespond = TRUE;
   }

   if (goodToRespond == TRUE)
   {
      SerialCommands_TransmitCockResponse(cockState);
      return TRUE;
   }
   else
   {
      return FALSE; //No commas where expected or something else wrong in input.. => can't trust this packet!
   }
}

/*COMMAND F, FIRE!!!! THEN REVERSE TO ALLOW RE-LATCH. (ALL USING A 180deg SERVO)
 #F,<DDDD>,<NNNN>,<EEEE>,<OOOO>\n
     - <DDDD> : UINT16 TimerTicksForSignalHighWhenFiring; hexadecimal fixed width (with no "0x"). Specifies the
                duty cycle by specificying the number of ticks of TIM3 per TIM3 period during which the
                signal shall be high when *firing* the gun.
     - <NNNN> : UINT16 NumPulsesForFiring. Specifies the number of pulses to actuate the motor with when firing.
     - <EEEE> : UINT16 TimerTicksForSignalHighWhenReversing; hexadecimal fixed width (with no "0x"). Specifies the
                duty cycle by specificying the number of ticks of TIM3 per TIM3 period during which the
                signal shall be high when *reversing* the gun's firing mechanism to allow autolatch.
     - <OOOO> : UINT16 NumPulsesForReversing. Specifies the number of pulses to actuate the motor with when reversing
                the gun's firing mechanism to allow autolatch*/
static BOOLEAN commandFire(UINT8 * commandBuffer, UINT8 commandSize)
{
   BOOLEAN goodToRespond = FALSE;
   UINT16  dutyCycleForFiring, numPulsesForFiring, dutyCycleForReversing, numPulsesForReversing;
   UINT8   fireState;

   if ( (commandBuffer[0] == DEFAULT_COMMAND_START_CHARACTER) &&
        (commandSize >= 22)                                   &&
        (commandBuffer[ 2] == ',')                            &&
        (commandBuffer[ 7] == ',')                            &&
        (commandBuffer[12] == ',')                            &&
        (commandBuffer[17] == ',')                               )
   {
      if ( !(extractSixteenBitNumber(&commandBuffer[3], &dutyCycleForFiring)) )
      {
         return FALSE;
      }
      if ( !(extractSixteenBitNumber(&commandBuffer[8], &numPulsesForFiring)) )
      {
         return FALSE;
      }
      if ( !(extractSixteenBitNumber(&commandBuffer[13], &dutyCycleForReversing)) )
      {
         return FALSE;
      }
      if ( !(extractSixteenBitNumber(&commandBuffer[18], &numPulsesForReversing)) )
      {
         return FALSE;
      }

      FireControl_FireAndReverse( dutyCycleForFiring    ,
                                  numPulsesForFiring    ,
                                  dutyCycleForReversing ,
                                  numPulsesForReversing   );

      fireState = 2; //2 means firing (see the enum above the FireState declaration in fire_control.c)

      goodToRespond = TRUE;
   }
   else if (commandBuffer[0] == DEFAULT_QUERY_START_CHARACTER)
   {
      fireState = FireControl_GetFireState();

      goodToRespond = TRUE;
   }

   if (goodToRespond == TRUE)
   {
      SerialCommands_TransmitFireResponse(fireState);
      return TRUE;
   }
   else
   {
      return FALSE; //No commas where expected or something else wrong in input.. => can't trust this packet!
   }
}

/*COMMAND R, GET STM8 CODE REV (OPTIONAL)
 #R\n*/
static BOOLEAN commandGetCodeRevision(UINT8 * commandBuffer, UINT8 commandSize)
{
   UINT8 * responseCommand;

   responseCommand = SerialBluetooth_RequestOutgoingPacketBuffer();
   if (responseCommand != NULL)
   {
      responseCommand[0]  = DEFAULT_RESPONSE_START_CHARACTER;
      responseCommand[1]  = 'R';
      responseCommand[2]  = ',';
      pushEightBitNumber( &(responseCommand[3]), SMARTILLERY_MAJOR_CODE_REV);
      responseCommand[5]  = ',';
      pushEightBitNumber( &(responseCommand[6]), SMARTILLERY_MINOR_CODE_REV);
      responseCommand[8] = DEFAULT_TERMINATION_CHARACTER;

      SerialBluetooth_MarkLastRequestedBufferForTransmit(9);
   }

   return TRUE;
}

/*COMMAND D, RETURN TIMERS TO DEFAULT SETTINGS AND CENTER THE BARREL (OPTIONAL)
 #D\n   : Return TIM2 and TIM3 to default settings (Prescaler 2, Modulus = 20000 => 1us tick, 20ms period), and attempt
          to center the gun barrel based on default settings. The STM8 will move pitch in the direction to get the ADC voltage
          to 2.4255V (0x0BC2), and will move the yaw motor at 7.5% (1500/20000) duty cycle for 5 seconds (250 pulses). */
static BOOLEAN commandDefaultAndCenter(UINT8 * commandBuffer, UINT8 commandSize)
{
   UINT8 * responseCommand;

   Timers_SetTim2ToDefaultPrescaler();
   Timers_SetTim2ToDefaultModulus();
   Timers_SetTim3ToDefaultPrescaler();
   Timers_SetTim3ToDefaultModulus();

   Yaw_ExecuteMove( 0x0000, 1500, 1000);
   Pitch_ExecuteMove( 0x0000, 0xFFFF, 0x0C56, 0xFFFF);

   responseCommand = SerialBluetooth_RequestOutgoingPacketBuffer();
   if (responseCommand != NULL)
   {
      responseCommand[0]  = DEFAULT_RESPONSE_START_CHARACTER;
      responseCommand[1]  = 'D';
      responseCommand[2] = DEFAULT_TERMINATION_CHARACTER;

      SerialBluetooth_MarkLastRequestedBufferForTransmit(3);
   }

   return TRUE;
}

/*COMMAND Q, QUERY READINESS STATE (OPTIONAL)
 ?Q\n */
static BOOLEAN commandQueryReadyState(UINT8 * commandBuffer, UINT8 commandSize)
{
   UINT8 * responseCommand;

   responseCommand = SerialBluetooth_RequestOutgoingPacketBuffer();
   if (responseCommand != NULL)
   {
      responseCommand[0]  = DEFAULT_RESPONSE_START_CHARACTER;
      responseCommand[1]  = 'Q';
      responseCommand[2]  = ',';
      pushEightBitNumber( &(responseCommand[3]), FireControl_GetReadinessState());
      responseCommand[5] = DEFAULT_TERMINATION_CHARACTER;

      SerialBluetooth_MarkLastRequestedBufferForTransmit(6);
   }

   return TRUE;
}

/*COMMAND V, READ THE ADC VOLTAGES (OPTIONAL)
 ?V\n */
static BOOLEAN commandQueryAdcVoltages(UINT8 * commandBuffer, UINT8 commandSize)
{
   UINT8 * responseCommand;
   UINT16  pitch, pitchStop, plungerStop;

   responseCommand = SerialBluetooth_RequestOutgoingPacketBuffer();
   if (responseCommand != NULL)
   {
      Adc_GetCurrentAdcVoltages(&pitch, &pitchStop, &plungerStop);

      responseCommand[0]  = DEFAULT_RESPONSE_START_CHARACTER;
      responseCommand[1]  = 'V';
      responseCommand[2]  = ',';
      pushSixteenBitNumber( &(responseCommand[3]),  pitch);
      responseCommand[7]  = ',';
      pushSixteenBitNumber( &(responseCommand[8]),  pitchStop);
      responseCommand[12]  = ',';
      pushSixteenBitNumber( &(responseCommand[13]), plungerStop);
      responseCommand[17] = DEFAULT_TERMINATION_CHARACTER;

      SerialBluetooth_MarkLastRequestedBufferForTransmit(18);
   }

   return TRUE;
}

/*COMMAND G, SET/GET TIM2 PRESCALER (OPTIONAL)   TIMER 2 IS USED FOR YAW AND COCK/SLACK
 #G,<PP>\n
     - <PP> : UINT8 Prescaler; hexadecimal fixed width (with no "0x"). Must be between 0 and 7.
              This is a power of 2 that determines the TIM2 tick by dividing the system clock value of 2MHz by 2^<PP>.*/
static BOOLEAN commandSetTim2Prescaler(UINT8 * commandBuffer, UINT8 commandSize)
{
   BOOLEAN goodToRespond = FALSE;
   UINT8   prescaler;
   UINT8 * responseCommand;

   if ( (commandBuffer[0] == DEFAULT_COMMAND_START_CHARACTER) &&
        (commandSize >= 4)                                    &&
        (commandBuffer[ 2] == ',')                               )
   {
      if ( !(extractEightBitNumber(&commandBuffer[3], &prescaler)) )
      {
         return FALSE;
      }

      Timers_SetTim2Prescaler(prescaler);

      goodToRespond = TRUE;
   }
   else if (commandBuffer[0] == DEFAULT_QUERY_START_CHARACTER)
   {
      prescaler = Timers_GetTim2Prescaler();

      goodToRespond = TRUE;
   }

   if (goodToRespond == TRUE)
   {
      responseCommand = SerialBluetooth_RequestOutgoingPacketBuffer();
      if (responseCommand != NULL)
      {
         responseCommand[0]  = DEFAULT_RESPONSE_START_CHARACTER;
         responseCommand[1]  = 'G';
         responseCommand[2]  = ',';
         pushEightBitNumber( &(responseCommand[3]),  prescaler);
         responseCommand[5]  = DEFAULT_TERMINATION_CHARACTER;

         SerialBluetooth_MarkLastRequestedBufferForTransmit(6);
      }
      return TRUE;
   }
   else
   {
      return FALSE; //No commas where expected or something else wrong in input.. => can't trust this packet!
   }
}

/*COMMAND H, SET/GET TIM2 MODULUS (OPTIONAL)   TIMER 2 IS USED FOR YAW AND COCK/SLACK
 #H,<MMMM>\n
     - <MMMM> : UINT16 Modulus; hexadecimal fixed width (with no "0x"). Determines the number of
                timer ticks per PWM period. */
static BOOLEAN commandSetTim2Modulus(UINT8 * commandBuffer, UINT8 commandSize)
{
   BOOLEAN goodToRespond = FALSE;
   UINT16  modulus;
   UINT8 * responseCommand;

   if ( (commandBuffer[0] == DEFAULT_COMMAND_START_CHARACTER) &&
        (commandSize >= 6)                                    &&
        (commandBuffer[ 2] == ',')                               )
   {
      if ( !(extractSixteenBitNumber(&commandBuffer[3], &modulus)) )
      {
         return FALSE;
      }

      Timers_SetTim2Modulus(modulus);

      goodToRespond = TRUE;
   }
   else if (commandBuffer[0] == DEFAULT_QUERY_START_CHARACTER)
   {
      modulus = Timers_GetTim2Modulus();

      goodToRespond = TRUE;
   }

   if (goodToRespond == TRUE)
   {
      responseCommand = SerialBluetooth_RequestOutgoingPacketBuffer();
      if (responseCommand != NULL)
      {
         responseCommand[0]  = DEFAULT_RESPONSE_START_CHARACTER;
         responseCommand[1]  = 'H';
         responseCommand[2]  = ',';
         pushSixteenBitNumber( &(responseCommand[3]),  modulus);
         responseCommand[7]  = DEFAULT_TERMINATION_CHARACTER;

         SerialBluetooth_MarkLastRequestedBufferForTransmit(8);
      }
      return TRUE;
   }
   else
   {
      return FALSE; //No commas where expected or something else wrong in input.. => can't trust this packet!
   }
}

/*COMMAND I, SET/GET TIM3 PRESCALER (OPTIONAL)   TIMER 3 IS USED FOR PITCH AND LOAD/FIRE
 #G,<PP>\n
     - <PP> : UINT8 Prescaler; hexadecimal fixed width (with no "0x"). Must be between 0 and 7.
              This is a power of 2 that determines the TIM2 tick by dividing the system clock value of 2MHz by 2^<PP>. */
static BOOLEAN commandSetTim3Prescaler(UINT8 * commandBuffer, UINT8 commandSize)
{
   BOOLEAN goodToRespond = FALSE;
   UINT8   prescaler;
   UINT8 * responseCommand;

   if ( (commandBuffer[0] == DEFAULT_COMMAND_START_CHARACTER) &&
        (commandSize >= 4)                                    &&
        (commandBuffer[ 2] == ',')                               )
   {
      if ( !(extractEightBitNumber(&commandBuffer[3], &prescaler)) )
      {
         return FALSE;
      }

      Timers_SetTim3Prescaler(prescaler);

      goodToRespond = TRUE;
   }
   else if (commandBuffer[0] == DEFAULT_QUERY_START_CHARACTER)
   {
      prescaler = Timers_GetTim3Prescaler();

      goodToRespond = TRUE;
   }

   if (goodToRespond == TRUE)
   {
      responseCommand = SerialBluetooth_RequestOutgoingPacketBuffer();
      if (responseCommand != NULL)
      {
         responseCommand[0]  = DEFAULT_RESPONSE_START_CHARACTER;
         responseCommand[1]  = 'I';
         responseCommand[2]  = ',';
         pushEightBitNumber( &(responseCommand[3]),  prescaler);
         responseCommand[5]  = DEFAULT_TERMINATION_CHARACTER;

         SerialBluetooth_MarkLastRequestedBufferForTransmit(6);
      }
      return TRUE;
   }
   else
   {
      return FALSE; //No commas where expected or something else wrong in input.. => can't trust this packet!
   }
}

/*COMMAND J, SET/GET TIM3 MODULUS (OPTIONAL)   TIMER 3 IS USED FOR PITCH AND LOAD/FIRE
 #H,<MMMM>\n
     - <MMMM> : UINT16 Modulus; hexadecimal fixed width (with no "0x"). Determines the number of
                timer ticks per PWM period. */
static BOOLEAN commandSetTim3Modulus(UINT8 * commandBuffer, UINT8 commandSize)
{
   BOOLEAN goodToRespond = FALSE;
   UINT16  modulus;
   UINT8 * responseCommand;

   if ( (commandBuffer[0] == DEFAULT_COMMAND_START_CHARACTER) &&
        (commandSize >= 6)                                    &&
        (commandBuffer[ 2] == ',')                               )
   {
      if ( !(extractSixteenBitNumber(&commandBuffer[3], &modulus)) )
      {
         return FALSE;
      }

      Timers_SetTim3Modulus(modulus);

      goodToRespond = TRUE;
   }
   else if (commandBuffer[0] == DEFAULT_QUERY_START_CHARACTER)
   {
      modulus = Timers_GetTim3Modulus();

      goodToRespond = TRUE;
   }

   if (goodToRespond == TRUE)
   {
      responseCommand = SerialBluetooth_RequestOutgoingPacketBuffer();
      if (responseCommand != NULL)
      {
         responseCommand[0]  = DEFAULT_RESPONSE_START_CHARACTER;
         responseCommand[1]  = 'J';
         responseCommand[2]  = ',';
         pushSixteenBitNumber( &(responseCommand[3]),  modulus);
         responseCommand[7]  = DEFAULT_TERMINATION_CHARACTER;

         SerialBluetooth_MarkLastRequestedBufferForTransmit(8);
      }
      return TRUE;
   }
   else
   {
      return FALSE; //No commas where expected or something else wrong in input.. => can't trust this packet!
   }
}

static BOOLEAN commandUnknown(UINT8 * commandBuffer, UINT8 commandSize)
{
   DEBUGGER_BREAKPOINT(); //Unknown command received
   return FALSE;
}

static BOOLEAN extractEightBitNumber(UINT8 * buffer, UINT8* numberPnt)
{
   BOOLEAN retVal;
   UINT8 i;
   UINT8 nibble;
   UINT8 eightBitNumber = 0;

   for (i=0; i<2; i++)
   {
      retVal = asciiToHexNibble(buffer[i], &nibble);
      if (!retVal)
      {
         return FALSE;
      }

      eightBitNumber |= ( nibble << ((1-i)*4) );
   }

   *numberPnt = eightBitNumber;
   return TRUE;
}

static BOOLEAN extractSixteenBitNumber(UINT8 * buffer, UINT16* numberPnt)
{
   BOOLEAN retVal;
   UINT8 i;
   UINT8 nibble;
   UINT16 sixteenBitNumber = 0;

   for (i=0; i<4; i++)
   {
      retVal = asciiToHexNibble(buffer[i], &nibble);
      if (!retVal)
      {
         return FALSE;
      }

      sixteenBitNumber |= ( ((UINT16)nibble) << ((3-i)*4) );
   }

   *numberPnt = sixteenBitNumber;
   return TRUE;
}

static BOOLEAN asciiToHexNibble(UINT8 asciiChar, UINT8 * nibble)
{
   if ( (asciiChar >= '0') && (asciiChar <= '9') )
   {
      *nibble = (asciiChar - '0');
      return TRUE;
   }
   else if ( (asciiChar >= 'A') && (asciiChar <= 'F') )
   {
      *nibble = ( (asciiChar - 'A') + 0x0A);
      return TRUE;
   }
   else if ( (asciiChar >= 'a') && (asciiChar <= 'f') )
   {
      *nibble = ( (asciiChar - 'a') + 0x0a);
      return TRUE;
   }

   return FALSE;
}

static void pushEightBitNumber(UINT8 * txBuffer, UINT8 numberToPush)
{
   UINT8 i, nibble;

   for (i=0; i<2; i++)
   {
      nibble = (numberToPush >> ((1-i)*4) ) & 0x0F;

      if (nibble < 10)
      {
         txBuffer[i] = nibble + '0';
      }
      else
      {
         txBuffer[i] = (nibble - 10) + 'A';
      }
   }
}

static void pushSixteenBitNumber(UINT8 * txBuffer, UINT16 numberToPush)
{
   UINT8 i, nibble;

   for (i=0; i<4; i++)
   {
      nibble = (UINT8) ( (numberToPush >> ((3-i)*4) ) & 0x0F );

      if (nibble < 10)
      {
         txBuffer[i] = nibble + '0';
      }
      else
      {
         txBuffer[i] = (nibble - 10) + 'A';
      }
   }
}
