/**
  ******************************************************************************
  * @file    main.c
  * @author  Microcontroller Division
  * @version V1.2.0
  * @date    09/2010
  * @brief   Main program body
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2010 STMicroelectronics</center></h2>
  */

// Includes ------------------------------------------------------------------
#include "stm8l15x.h"
#include "stm8l_discovery_lcd.h"
#include "discover_board.h"
#include "stdtypes.h"
#include "scheduler.h"
#include "serial_1_driver.h"
#include "serial_2_bluetooth.h"
#include "yaw.h"
#include "pitch.h"
#include "fire_control.h"
#include "timer.h"
#include "adc.h"

// Machine status used by main for active function set by user button in interrupt handler
UINT8 StateMachine;

// LCD bar graph: used for display active function
extern UINT8 t_bar[2];

static void initHardware(void);
static void initSoftware(void);

static UINT8 GlobalVar = 0;
/**
  * @brief main entry point.
  * @par Parameters None
  * @retval void None
  * @par Required preconditions: None
  */
void main(void)
{
   initHardware();
   initSoftware();

   enableInterrupts();

   while (1)
   {
      RunScheduler();

      if (GlobalVar)
      {
        SerialBluetooth_ConfigureModule();
        GlobalVar = 0;
      }
   }
}

static void initHardware(void)
{
   // USER button init: GPIO set in input interrupt active mode
   GPIO_Init( BUTTON_GPIO_PORT, USER_GPIO_PIN, GPIO_Mode_In_FL_IT);

   // Green led init: GPIO set in output
   GPIO_Init( LED_GREEN_PORT, LED_GREEN_PIN, GPIO_Mode_Out_PP_High_Fast);

   // Blue led init: GPIO set in output
   GPIO_Init( LED_BLUE_PORT, LED_BLUE_PIN, GPIO_Mode_Out_PP_High_Fast);

   // USART1_TX init on pin C5
   GPIO_Init( GPIOC, GPIO_Pin_5, GPIO_Mode_Out_PP_High_Fast);
   // USART1_RX init on pin C6
   GPIO_Init( GPIOC, GPIO_Pin_6, GPIO_Mode_In_FL_No_IT);

   // TIM2_CH1 (Yaw) on pin B0
   GPIO_Init( GPIOB, GPIO_Pin_0, GPIO_Mode_Out_PP_High_Fast);
   // TIM3_CH1 (Pitch) on pin B1
   GPIO_Init( GPIOB, GPIO_Pin_1, GPIO_Mode_Out_PP_High_Fast);
   // TIM2_CH2 (Cock/latch/slack) on pin B2
   GPIO_Init( GPIOB, GPIO_Pin_2, GPIO_Mode_Out_PP_High_Fast);
   // TIM3_CH2 (Fire/reverse) on pin D0
   GPIO_Init( GPIOD, GPIO_Pin_0, GPIO_Mode_Out_PP_High_Fast);

   //ADC1_IN0 (trimpot on pitch sense) on pin A6
   GPIO_Init( GPIOA, GPIO_Pin_6, GPIO_Mode_In_FL_No_IT);
   //ADC1_IN1 (current sense on plunger stop) on pin A5
   GPIO_Init( GPIOA, GPIO_Pin_5, GPIO_Mode_In_FL_No_IT);
   //ADC1_IN2 (current send on pitch stop) on pin A4
   GPIO_Init( GPIOA, GPIO_Pin_4, GPIO_Mode_In_FL_No_IT);

   // Initializes the LCD glass
   LCD_GLASS_Init();

   // Initialize the USART (communication to bluetooth module)
   SerialDriver_Init();

   // Initialize the TIM2 (PWM for yaw and cock/slack), TIM3 (PWM for pitch and fire/reverse)
   // and TIM4 (scheduler tick) timers
   Timers_Init();

   // Initializes ADC
   Adc_Init();
}

static void initSoftware(void)
{
   // Switch off the leds at start
   GPIO_LOW(LED_GREEN_PORT, LED_GREEN_PIN);
   GPIO_LOW(LED_BLUE_PORT,  LED_BLUE_PIN);

   // Welcome display
   LCD_GLASS_ScrollSentence("      ** SMARTILLERY **",1,SCROLL_SPEED);
   //LCD_BlinkConfig(LCD_BlinkMode_AllSEG_AllCOM,LCD_BlinkFrequency_Div512);
   //LCD_GLASS_DisplayString("SMARTILL");
   //delay_ms(500);
   //LCD_BlinkConfig(LCD_BlinkMode_Off,LCD_BlinkFrequency_Div32);

   // LED Green ON
   GPIO_HIGH(LED_GREEN_PORT,LED_GREEN_PIN);

   // Init Bar on LCD all are OFF --> first function VDD displays
   BAR0_OFF;
   BAR1_OFF;
   BAR2_OFF;
   BAR3_OFF;

   //Initialize the scheduler
   InitializeScheduler();

   //Initialize the bluetooth network layer :-)
   SerialBluetooth_Init();

   //Initialize Yaw Engine
   Yaw_Init();

   //Initialize Pitch Engine
   Pitch_Init();

   //Initialize Fire Control Engine
   FireControl_Init();
}

/*
*----------------------------------------------------------------------
*  void DebuggerBreakpoint(void)
*
*  Summary:    STM8 micro instruction set does not support a debugger
*              breakpoint instruction like our dear old HC9S08 friends
*              do, but it does allow plenty of breakpoints, so leave
*              a permanent breakpoint here and you get the same affect
*              (Almost. You will have to step out or look at the call
*              stack to see which DEBUGGER_BREAKPOINT() this is)
*
*  Input:      None
*
*  Output:     None
*
*  Notes:      None
*
*----------------------------------------------------------------------
*/
void DebuggerBreakpoint(void)
{
   NOP();
}

//******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE***
