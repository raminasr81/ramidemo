#include "stm8l15x.h"
#include "stdtypes.h"
#include "serial_3_commands.h"
#include "utility.h"
#include "pitch.h"

static INT16 PitchAngle;
static BOOLEAN MovingPitch;

void Pitch_Init(void)
{
   PitchAngle = 0xFFFF; //Unknown
   MovingPitch = FALSE;
}

void Pitch_ExecuteMove( INT16 angle, UINT16 dutyCycle, UINT16 adcVoltage, UINT16 numPulses)
{
   PitchAngle = angle;

   //RDN TODO

   MovingPitch = TRUE;
}

void Pitch_PitchCountdownExpired(void)
{
}

INT16 Pitch_GetPitchAngle(void)
{
   return PitchAngle;
}

BOOLEAN Pitch_GetPitchState(void)
{
   return MovingPitch;
}




