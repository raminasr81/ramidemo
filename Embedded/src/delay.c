/**
  ******************************************************************************
  * @file    delay.c
  * @author  Microcontroller Division
  * @version V1.2.0
  * @date    09/2010
  * @brief   delay functions
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2010 STMicroelectronics</center></h2>
  */

/* Includes ------------------------------------------------------------------*/

#include "stm8l15x_clk.h"
/**
  * @brief  delay for some time in ms unit. Thrown off by interrupts
  * @caller
  * @param  n_ms is how many ms of time to delay
  * @retval None
  */
void delay_ms(u16 n_ms)
{
   u8 timerCaptureVal;

   //Use TIM4, a timer that ticks every 8us and has an auto reload value of 250.
   //Whenever we wrap around to the same value of TIM4, we know 2ms have passed.

   n_ms >>= 1; //Divide by 2 to get from units of 1ms to units of 2ms
   if (n_ms == 0)
   {
      n_ms = 1;
   }

   timerCaptureVal = TIM4->CNTR; //Capture the current time

   while (n_ms > 0)
   {
      //Wait for TIM4->CNTR to tick up away from timerCaptureVal
      while (TIM4->CNTR == timerCaptureVal)
      {
         //Do nothing
      }

      //Now wait for TIM4->CNTR to tick up to 250 and then wrap back around to timerCaptureVal
      while (TIM4->CNTR != timerCaptureVal)
      {
         //Do nothing
      }

      n_ms--;
   }
}

/**
  * @brief  delay for some time in 8us units (partially accurate, thrown off by interrupts)
  * @caller
  * @param n_8us is how many 8us units of time to delay
  * @retval None
  */
void delay_8us(u16 n_8us)
{
   u8 timerCaptureVal;

   //Use TIM4, a timer that ticks every 8us

   while (n_8us > 0)
   {
      timerCaptureVal = TIM4->CNTR; //Capture the current time

      //Wait for TIM4->CNTR to tick up away from timerCaptureVal
      while (TIM4->CNTR == timerCaptureVal)
      {
         //Do nothing
      }

      n_8us--;
   }
}

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
