#include "stm8l15x.h"
#include "stdtypes.h"
#include "serial_1_driver.h"
#include "serial_2_bluetooth.h"
#include "utility.h"
#include "scheduler.h"

//Communicates full duplex with Bluetooth module. Params are:
// 9600  bps (baud)
//    8  Data Bits
//    1  Stop Bit
//   No  Parity
//   No  Handshake/Flow Control

typedef enum
{
   SERIAL_DRIVER_TX_STATE__IDLE,
   SERIAL_DRIVER_TX_STATE__TRANSMITTING,
   SERIAL_DRIVER_TX_STATE__WAITING_FOR_TX_COMPLETE
} SERIAL_DRIVER_TX_STATE;

static UINT8 TxPacketBuffer[MAX_SERIAL_PACKET_SIZE];
static UINT8 TxPacketBufferSize = 0;
static UINT8 TxPacketBufferIndex = 0;
static SERIAL_DRIVER_TX_STATE TxState = SERIAL_DRIVER_TX_STATE__IDLE;

typedef enum
{
   SERIAL_DRIVER_RX_STATE__IDLE,
   SERIAL_DRIVER_RX_STATE__RECEIVING,
   SERIAL_DRIVER_RX_STATE__WAITING_FOR_PACKET_TO_BE_PROCESSED
} SERIAL_DRIVER_RX_STATE;

static UINT8 RxPacketBuffer[MAX_SERIAL_PACKET_SIZE];
static UINT8 RxPacketBufferSize = 0;
static SERIAL_DRIVER_RX_STATE RxState = SERIAL_DRIVER_RX_STATE__IDLE;

static UINT8 RxPacketCommandStartCharacter;
static UINT8 RxPacketQueryStartCharacter;
static UINT8 RxPacketTerminationCharacter;
static UINT8 RxPacketTerminationCharacterDuplicatesAllowed;

void SerialDriver_Init(void)
{
   USART_Mode_TypeDef mode;

   //Default start character to '#' and terminate character to '\n'
   RxPacketCommandStartCharacter                 = DEFAULT_COMMAND_START_CHARACTER;
   RxPacketQueryStartCharacter                   = DEFAULT_QUERY_START_CHARACTER;
   RxPacketTerminationCharacter                  = DEFAULT_TERMINATION_CHARACTER;
   RxPacketTerminationCharacterDuplicatesAllowed = 0;

   //Clock USART1
   CLK_PeripheralClockConfig(CLK_Peripheral_USART1, ENABLE);

   //Route USART1 to ports PC5 and PC6
   SYSCFG->RMPCR1 &= ~(SYSCFG_RMPCR1_USART1TR_REMAP);
   SYSCFG->RMPCR1 |=  (0x20);

   //Initialize UART @ 9600bps, 1 stop bit, no parity, full duplex
   mode  = USART_Mode_Rx;
   mode |= USART_Mode_Tx;
   USART_Init(USART1, 9600, USART_WordLength_8b, USART_StopBits_1,
              USART_Parity_No, mode);

   //Enable receiver and receive interrupt
   USART1->CR2 |= (USART_CR2_REN | USART_CR2_RIEN);

   //Turn off transmit interrupts, but enable the transmitter
   USART1->CR2 &= ~(USART_CR2_TIEN);  //Disable transmit interrupt
   USART1->CR2 &= ~(USART_CR2_TCIEN); //Disable transmit complete interrupt
   USART1->SR  &= ~(USART_SR_TC);     //Clear transmit complete
   USART1->CR2 |= USART_CR2_TEN;      //Enable transmitter

   //Enable error interrupts
   USART1->CR2 |= USART_CR5_EIE;

   //Enable module
   USART_Cmd(USART1, ENABLE);
}

void SerialDriver_RxInterrupt(void)
{
   UINT8 statusRegister;
   UINT8 dataRegister;

   statusRegister = USART1->SR;
   dataRegister   = USART1->DR;

   if (statusRegister & USART_SR_NF) //Noise Flag
   {
      DEBUGGER_BREAKPOINT(); //Getting noise.. fix your soldering fool.. can't hear myself think
   }

   if ( (statusRegister & USART_SR_OR) ||   //Overrun!!
        (statusRegister & USART_SR_FE) ||   //Framing Error
        (statusRegister & USART_SR_PE)    ) //Parity Error!!
   {
      //Flush the toilet
      RxPacketBufferSize = 0;
      RxState = SERIAL_DRIVER_RX_STATE__IDLE;

      DEBUGGER_BREAKPOINT();
   }
   else if (statusRegister & USART_SR_RXNE)
   {
      if (RxState == SERIAL_DRIVER_RX_STATE__WAITING_FOR_PACKET_TO_BE_PROCESSED)
      {  //Ignore this byte since we're not done processing the last packet!
         DEBUGGER_BREAKPOINT(); //Received a byte before we were done processing last packet
      }
      else
      {
         if (RxPacketCommandStartCharacter != NO_PACKET_START_CHARACTER_EXPECTED)
         {  //We're looking for a particular start character
            if ( (dataRegister == RxPacketCommandStartCharacter) ||
                 (dataRegister == RxPacketQueryStartCharacter)      )
            {  //This is the expected start character!

               if (RxState != SERIAL_DRIVER_RX_STATE__IDLE)
               {
                  DEBUGGER_BREAKPOINT();
               }

               RxPacketBufferSize = 0;
            }
         }

         RxPacketBuffer[RxPacketBufferSize++] = dataRegister;

         if ( (dataRegister == RxPacketTerminationCharacter) && (RxPacketTerminationCharacterDuplicatesAllowed == 0) )
         {
            EnableTaskFromInterrupt(SERIAL_BLUETOOTH_PACKET_RECEIVED_TASK);
            RxState = SERIAL_DRIVER_RX_STATE__WAITING_FOR_PACKET_TO_BE_PROCESSED;
         }
         else
         {
            if (dataRegister == RxPacketTerminationCharacter)
            {
               RxPacketTerminationCharacterDuplicatesAllowed--;
            }

            if (RxPacketBufferSize >= MAX_SERIAL_PACKET_SIZE)
            {  //Reached 100 bytes without a packet terminator
               DEBUGGER_BREAKPOINT(); //Incoming RX data violated max packet size!
               RxState = SERIAL_DRIVER_RX_STATE__IDLE;
               RxPacketBufferSize = 0;
            }
            else
            {
               RxState = SERIAL_DRIVER_RX_STATE__RECEIVING;
            }
         }
      }
   }
   else
   {
      DEBUGGER_BREAKPOINT(); //Why are we here? Dude, where's my car?
   }
}

//Sets the character that we will consider the 'start delimiter' for an incoming packet
void SerialDriver_SetRxPacketStartCharacters(UINT8 newRxPacketCommandStartCharacter,
                                             UINT8 newRxPacketQueryStartCharacter   )
{
   BEGIN_CRITICAL_SECTION();
   RxPacketCommandStartCharacter = newRxPacketCommandStartCharacter;
   RxPacketQueryStartCharacter   = newRxPacketQueryStartCharacter;
   END_CRITICAL_SECTION();
}

//Sets the character that we will consider the 'terminator' for an incoming packet
//duplicatesExpected should ideally be 0. If you expect the incoming packet to have two instances of the terminating
//character (for example in "OKsetname" the terminator is an 'e' and there are two of them), you can set
//duplicatesExpected to 1.
void SerialDriver_SetRxPacketTerminationCharacter(UINT8 newRxPacketTerminationCharacter, UINT8 duplicatesExpected)
{
   BEGIN_CRITICAL_SECTION();
   RxPacketTerminationCharacter = newRxPacketTerminationCharacter;
   RxPacketTerminationCharacterDuplicatesAllowed = duplicatesExpected;
   END_CRITICAL_SECTION();
}

//Sets the target of a passed in pointer to the RxPacketBufferSize, and returns a pointer to RxPacketBuffer
UINT8 * SerialDriver_GetRxPacket(UINT8 * packetSize)
{
   if (RxState != SERIAL_DRIVER_RX_STATE__WAITING_FOR_PACKET_TO_BE_PROCESSED)
   {
      DEBUGGER_BREAKPOINT();
      *packetSize = 0;
      return NULL;
   }

   *packetSize = RxPacketBufferSize;
   return RxPacketBuffer;
}

//Restores RxState to idle since we're done processing the received packet
void SerialDriver_NotifyDoneProcessingPacket(void)
{
   if (RxState != SERIAL_DRIVER_RX_STATE__WAITING_FOR_PACKET_TO_BE_PROCESSED)
   {
      DEBUGGER_BREAKPOINT();
   }
   else
   {
      RxState = SERIAL_DRIVER_RX_STATE__IDLE;
      RxPacketBufferSize = 0;
   }
}

void SerialDriver_TxInterrupt(void)
{
   UINT8 statusRegister = USART1->SR;

   if ( (!(statusRegister & USART_SR_TXE)) &&
        (!(statusRegister & USART_SR_TC ))    )
   {
      DEBUGGER_BREAKPOINT(); //Why are we here? What is the meaning of life? This is depressing.
   }

   if (TxState == SERIAL_DRIVER_TX_STATE__TRANSMITTING)
   {
      if (statusRegister & USART_SR_TXE) //Transmit empty
      {
         if (TxPacketBufferIndex >= TxPacketBufferSize)
         {
            DEBUGGER_BREAKPOINT(); //Why did we enable transmit empty when we're not in the packet transmitting state?
            TxState = SERIAL_DRIVER_TX_STATE__IDLE;
            USART1->CR2 &= ~(USART_CR2_TIEN);  //Disable transmit interrupt
            USART1->CR2 &= ~(USART_CR2_TCIEN); //Disable transmit complete interrupt
            USART1->SR  &= ~(USART_SR_TC);     //Clear transmit complete
         }
         else
         {
            USART1->DR = TxPacketBuffer[TxPacketBufferIndex++];
            if (TxPacketBufferIndex >= TxPacketBufferSize)
            {
               //We just stuffed the last byte of the outgoing packet in the data buffer.
               //Switch to the 'waiting for transmit complete' state
               TxState = SERIAL_DRIVER_TX_STATE__WAITING_FOR_TX_COMPLETE;
               USART1->CR2 &= ~(USART_CR2_TIEN); //Disable transmit interrupt
               USART1->CR2 |= USART_CR2_TCIEN;   //Enable transmit complete interrupt
            }
         }
      }
      else
      {
         DEBUGGER_BREAKPOINT(); //Expecting a transmit empty but got a transmit complete (without a transmit empty!!?)
      }
   }
   else if (TxState == SERIAL_DRIVER_TX_STATE__WAITING_FOR_TX_COMPLETE)
   {
      if (statusRegister & USART_SR_TC) //Transmit complete!
      {
         if (TxPacketBufferIndex < TxPacketBufferSize)
         {
            DEBUGGER_BREAKPOINT(); //Why did we enable transmit complete when we're not at the end of the packet?
         }

         USART1->CR2 &= ~(USART_CR2_TIEN);  //Disable transmit interrupt
         USART1->CR2 &= ~(USART_CR2_TCIEN); //Disable transmit complete interrupt
         USART1->SR  &= ~(USART_SR_TC);     //Clear transmit complete

         TxState = SERIAL_DRIVER_TX_STATE__IDLE;
      }
      else
      {
         DEBUGGER_BREAKPOINT(); //Expecting a transmit complete but got a transmit empty
      }
   }
   else //if (TxState == SERIAL_DRIVER_TX_STATE__IDLE)
   {
      DEBUGGER_BREAKPOINT(); //Spurious transmit interrupt!!

      USART1->CR2 &= ~(USART_CR2_TIEN);  //Disable transmit interrupt
      USART1->CR2 &= ~(USART_CR2_TCIEN); //Disable transmit complete interrupt
      USART1->SR  &= ~(USART_SR_TC);     //Clear transmit complete
   }
}

BOOLEAN SerialDriver_IsTxIdle(void)
{
   if (TxState == SERIAL_DRIVER_TX_STATE__IDLE)
   {
      return TRUE;
   }

   return FALSE;
}

void SerialDriver_TransmitPacket(const UINT8 * packetToTransmit, UINT8 packetSize)
{
   if (TxState != SERIAL_DRIVER_TX_STATE__IDLE)
   {
      DEBUGGER_BREAKPOINT(); //Trying to transmit a packet when there is already one transmitting!
      return;
   }

   if (packetSize == 0)
   {
      DEBUGGER_BREAKPOINT(); //Trying to transmit a zero sized packet? Disappointing.
      return;
   }

   TxPacketBufferIndex = 0;
   MemCopy( (void *)TxPacketBuffer, (void *)packetToTransmit, packetSize);
   TxPacketBufferSize = packetSize;
   TxState = SERIAL_DRIVER_TX_STATE__TRANSMITTING;


   USART1->CR2 &= ~(USART_CR2_TCIEN); //Disable transmit complete interrupt
   USART1->SR  &= ~(USART_SR_TC);     //Clear transmit complete in case its set
   USART1->CR2 |= USART_CR2_TIEN;     //Enable transmit interrupt
}
