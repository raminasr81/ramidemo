#include "stm8l15x.h"
#include "stdtypes.h"
#include "serial_3_commands.h"
#include "utility.h"
#include "yaw.h"
#include "timer.h"

static INT16 YawAngle;
static BOOLEAN MovingYaw;

void Yaw_Init(void)
{
   YawAngle = 0xFFFF; //Unknown
   MovingYaw = FALSE;
}

void Yaw_ExecuteMove( INT16 angle, UINT16 dutyCycle, UINT16 numPulses)
{
   YawAngle = angle;

   Timers_SetYawPwm(dutyCycle, numPulses);

   MovingYaw = TRUE;
}

void Yaw_YawCountdownExpired(void)
{
   MovingYaw = FALSE;
   SerialCommands_TransmitYawResponse(YawAngle, (UINT8)MovingYaw);
}

INT16 Yaw_GetYawAngle(void)
{
   return YawAngle;
}

BOOLEAN Yaw_GetYawState(void)
{
   return MovingYaw;
}


