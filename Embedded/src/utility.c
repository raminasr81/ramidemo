/*
*------------------------------------------------------------------------------
*   Include Files
*------------------------------------------------------------------------------
*/

#include "stm8l15x.h"
#include "stdtypes.h"
#include "utility.h"

/*
*------------------------------------------------------------------------------
*   Private Defines
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Private Macros
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Private Data Types
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Public Variables
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Private Variables (static)
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Public Constants
*------------------------------------------------------------------------------
*/

// Mask lookup table into 8 bits.
const UINT8  BIT8_ARRAY[8] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
const UINT16 BIT16_ARRAY[16] = {
   0x0001, 0x0002, 0x0004, 0x0008,
   0x0010, 0x0020, 0x0040, 0x0080,
   0x0100, 0x0200, 0x0400, 0x0800,
   0x1000, 0x2000, 0x4000, 0x8000};

const UINT32 BIT32_ARRAY[32] = {
   0x00000001, 0x00000002, 0x00000004, 0x00000008,
   0x00000010, 0x00000020, 0x00000040, 0x00000080,
   0x00000100, 0x00000200, 0x00000400, 0x00000800,
   0x00001000, 0x00002000, 0x00004000, 0x00008000,
   0x00010000, 0x00020000, 0x00040000, 0x00080000,
   0x00100000, 0x00200000, 0x00400000, 0x00800000,
   0x01000000, 0x02000000, 0x04000000, 0x08000000,
   0x10000000, 0x20000000, 0x40000000, 0x80000000};


/*
*------------------------------------------------------------------------------
*   Private Constants (static)
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Private Function Prototypes (static)
*------------------------------------------------------------------------------
*/

/*
*==============================================================================
*  Public Functions
*==============================================================================
*/

void MemCopy(void *Dst, const void *Src, UINT8 Length)
{
   if (Length == 0)
   {
      return;
   }

   void *endPtr = ((UINT8*)Dst) + Length;

   // Pre-decrement the source pointer to account for the pre-increment
   // in the loop.
   Src = ((UINT8*)Src) - 1;

   do
   {
      Src = ((UINT8*)Src) + 1;
      *((UINT8*)Dst) = *((UINT8*)Src);
      Dst = ((UINT8*)Dst) + 1;
   }
   while (endPtr != Dst);
}

void MemCopyReverse(void *Dst, const void *Src, UINT8 Length)
{
   UINT8 * dst_ptr = Dst;
   UINT8 const * src_ptr = Src;
   UINT8 *end_ptr = dst_ptr;

   dst_ptr += Length;
   // pre-increment the source pointer to account for the pre-decrement
   // in the loop.
   src_ptr += Length;

   while (end_ptr != dst_ptr)
   {
      *--dst_ptr = *--src_ptr;
   }
}

/*
*==============================================================================
*  MemClear()
*
*  Summary:    This function clears length bytes of memory pointed to by Dst
*
*  Input:      void *Dst - pointer to memory to clear
*              UINT8 Length - number of bytes to clear
*
*  Output:     None
*
*  Notes:      None
*
*==============================================================================
*/
void MemClear(void *Dst, UINT8 Length)
{
   if (Length & 1)
   {
      // Odd Number of Bytes to clear
      *((UINT8*)Dst) = 0;
      Dst = ((UINT8*)Dst) + 1;
      Length--;
   }
   // Even number of bytes..
   while (Length)
   {
      *(((UINT16*)Dst)) = 0;
      Dst = ((UINT16*)Dst) + 1;
      Length -= 2;
   }
}

/*
*==============================================================================
*  MemSet()
*
*  Summary:    This function sets Length bytes of memory pointed to by Dst
*              equal to value
*
*  Input:      void *Dst - pointer to memory to fill
*              UINT8 Length - number of bytes to clear
*              UINT8 Value - value to fill memory with
*
*  Output:     None
*
*  Notes:      None
*
*==============================================================================
*/
void MemSet(void *Dst, UINT8 Length, UINT8 Value)
{
   UINT8 * dst_ptr = (UINT8 *)Dst;

   for (; Length > 0; Length--)
   {
      *dst_ptr = Value;
      ++dst_ptr;
   }
}

/*
*==============================================================================
*  MemCompare()
*
*  Summary:    This function compares two length long blocks of memory.
*
*  Input:      void *Src1 - pointer to first block of memory
*              void *Src2 - pointer to second block of memory
*              UINT8 Length - number of bytes to compare
*
*  Output:     None
*
*  Notes:      None
*
*==============================================================================
*/

INT8 MemCompare(const void *Src1, const void *Src2, UINT8 Length)
{
   UINT8 const * src1_ptr = (UINT8 const *)Src1;
   UINT8 const * src2_ptr = (UINT8 const *)Src2;

   for (; Length > 0; Length--)
   {
      if (*src1_ptr != *src2_ptr)
      {
         if (*src1_ptr < *src2_ptr)
         {
            return COMPARE_LESS;
         }
         else
         {
            return COMPARE_GREATER;
         }
      }
      ++src1_ptr;
      ++src2_ptr;
   }

   return (COMPARE_EQUAL);
}

/*
*==============================================================================
*  MemCompareEqual()
*
*  Summary:    This function compares two length long blocks of memory.
*
*  Input:      void *Src1 - pointer to first block of memory
*              void *Src2 - pointer to second block of memory
*              UINT8 Length - number of bytes to compare
*
*  Output:     None
*
*  Notes:      None
*
*==============================================================================
*/
BOOLEAN MemCompareEqual(const void *Src1, const void *Src2, UINT8 Length)
{
   UINT8 const * src1_ptr = (UINT8 const *)Src1;
   UINT8 const * src2_ptr = (UINT8 const *)Src2;

   for (; Length > 0; Length--)
   {
      if (*src1_ptr != *src2_ptr)
      {
         return (FALSE);
      }
      ++src1_ptr;
      ++src2_ptr;
   }

   return (TRUE);
}


/*
*==============================================================================
*  MemCompareLessThan()
*
*  Summary:    This function compares two length long blocks of memory to
*              determine if the first block is less than the second block.
*
*  Input:      void *Src1 - pointer to first block of memory
*              void *Src2 - pointer to second block of memory
*              UINT8 Length - number of bytes to compare
*
*  Output:     TRUE = first block is less than second block
*
*              FALSE = first block is NOT less than second block
*
*  Notes:      None
*
*==============================================================================
*/
BOOLEAN MemCompareLessThan(const void *Src1, const void *Src2, UINT8 Length)
{
   UINT8 const * src1_ptr = (UINT8 const *)Src1;
   UINT8 const * src2_ptr = (UINT8 const *)Src2;

   for (; Length > 0; Length--)
   {
      if (*src1_ptr < *src2_ptr)
      {
         return (TRUE);
      }
      ++src1_ptr;
      ++src2_ptr;
   }

   return (FALSE);
}

/*
*------------------------------------------------------------------------------
*  Mult8BitPercent()
*
*  Summary:    This routine is used to multiply a 16-bit value by an 8-bit
*              percentage (x/256 %).
*
*  Input:      UINT16 value - original value
*              UINT8 percent - percentage to multiply value by
*
*  Output:     UINT16 result of (value * percent) / 256
*
*  Notes:      NONE
*
*------------------------------------------------------------------------------
*/
UINT16 Mult8BitPercent(UINT16 value, UINT8 percent)
{
   UINT16 result;
   UNION16 temp;

   // First, multiply the percent with high byte of the value and store the
   // result.  This value actually represents the two most significant bytes
   // of the 24bit answer.
   result = ((UNION16 *)&value)->u8.h * percent;

   // Second, multiply the percent with the low byte of the value, but only
   // add the hi byte of the operation to "result" since we are only concerned
   // with the two most significant bytes of the 24bit answer.
   temp.u16 = ((UNION16 *)&value)->u8.l * percent;
   result += temp.u8.h;

   return result;
}

/*
*==============================================================================
*  Private Functions
*==============================================================================
*/

/*
*  End of $RCSfile: utility.c $
*/
