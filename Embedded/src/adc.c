#include "stm8l15x.h"
#include "stdtypes.h"
#include "serial_3_commands.h"
#include "utility.h"
#include "adc.h"

static UINT16 CurrentAdcVoltageForPitch       = 0;
static UINT16 CurrentAdcVoltageForPitchStop   = 0;
static UINT16 CurrentAdcVoltageForPlungerStop = 0;

void Adc_Init(void)
{
   // Enable ADC clock
   CLK_PeripheralClockConfig(CLK_Peripheral_ADC1, ENABLE);

   //ADC_Icc_Init(); //To get this function back, see icc_measure.c in graveyard
}

void Adc_ConversionTask(void)
{
   //RDN TODO ADC CONVERSION
}

void Adc_GetCurrentAdcVoltages(UINT16 * pitch, UINT16 * pitchStop, UINT16 * plungerStop)
{
   *pitch       = CurrentAdcVoltageForPitch;
   *pitchStop   = CurrentAdcVoltageForPitchStop;
   *plungerStop = CurrentAdcVoltageForPlungerStop;
}

