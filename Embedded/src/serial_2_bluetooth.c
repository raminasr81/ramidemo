#include "stm8l15x.h"
#include "stdtypes.h"
#include "serial_1_driver.h"
#include "serial_2_bluetooth.h"
#include "serial_3_commands.h"
#include "scheduler.h"

#define CONFIG_MODULE_TIMEOUT_IN_10_MS_UNITS  (250) //2.5 seconds in 10ms units

#define NUM_OUTGOING_PACKET_BUFFERS             (5)

#define INVALID_OUTGOING_PACKET_BUFFER       (0xFF)

typedef enum
{
   SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__IDLE,
   SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__STARTING_TO_CONFIGURE,
   SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__AWAITING_DEVICE_NAME_ACK,
   SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__AWAITING_PASS_KEY_ACK
} SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE;

static SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE ConfigModuleState = SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__IDLE;

static UINT8 ConfigModuleTimeout;

static UINT8 OutgoingPacketBuffers[NUM_OUTGOING_PACKET_BUFFERS][MAX_SERIAL_PACKET_SIZE];
static UINT8 OutgoingPacketBuffersSize[NUM_OUTGOING_PACKET_BUFFERS];
static UINT8 OutgoingPacketBufferScanIndex = 0;
static UINT8 OutgoingPacketBufferLastReserved;

#define CONFIG_PACKET_SET_NAME__SIZE  (18)
const UINT8 ConfigPacketSetName[CONFIG_PACKET_SET_NAME__SIZE] =
{
   'A', 'T', '+', 'N', 'A', 'M', 'E',
   'S', 'M', 'A', 'R', 'T', 'I', 'L', 'L', 'E', 'R', 'Y'
};

#define CONFIG_PACKET_SET_PASS_KEY__SIZE  (10)
const UINT8 ConfigPacketSetPassKey[CONFIG_PACKET_SET_PASS_KEY__SIZE] =
{
   'A', 'T', '+', 'P', 'I', 'N',
   '1', '4', '0', '7'
};

static void serialBluetooth_handleDeviceNameAck(UINT8 * rxPacketPnt, UINT8 rxPacketSize);
static void serialBluetooth_handlePassKeyAck(UINT8 * rxPacketPnt, UINT8 rxPacketSize);

void SerialBluetooth_Init(void)
{
   UINT8 i;

   for (i=0; i<NUM_OUTGOING_PACKET_BUFFERS; i++)
   {
      OutgoingPacketBuffersSize[i] = 0;
   }

   OutgoingPacketBufferLastReserved = INVALID_OUTGOING_PACKET_BUFFER;
}

void SerialBluetooth_PacketReceivedTask(void)
{
   UINT8 * rxPacketPnt;
   UINT8   rxPacketSize;

   rxPacketPnt = SerialDriver_GetRxPacket(&rxPacketSize);

   if (ConfigModuleState == SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__AWAITING_DEVICE_NAME_ACK)
   {
      serialBluetooth_handleDeviceNameAck(rxPacketPnt, rxPacketSize);
   }
   else if (ConfigModuleState == SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__AWAITING_PASS_KEY_ACK)
   {
      serialBluetooth_handlePassKeyAck(rxPacketPnt, rxPacketSize);
   }
   else
   {
      if ( (rxPacketSize < 3)                                             ||
           ( (rxPacketPnt[0] != DEFAULT_COMMAND_START_CHARACTER) &&
             (rxPacketPnt[0] != DEFAULT_QUERY_START_CHARACTER)      )     ||
           (rxPacketPnt[rxPacketSize-1] != DEFAULT_TERMINATION_CHARACTER)    )
      {
         DEBUGGER_BREAKPOINT();
      }
      else
      {
         SerialCommands_HandleReceivedCommand(rxPacketPnt, rxPacketSize);
         NotifyLedEngineCommandReceived();
      }
   }

   SerialDriver_NotifyDoneProcessingPacket();
}

//10ms periodic task
void SerialBluetooth_TxManagerTask(void)
{
   if (ConfigModuleState != SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__IDLE)
   {
      if ( (ConfigModuleState == SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__STARTING_TO_CONFIGURE) &&
           (SerialDriver_IsTxIdle())                                                                )
      {
         SerialDriver_SetRxPacketStartCharacters(NO_PACKET_START_CHARACTER_EXPECTED, 0);
         SerialDriver_SetRxPacketTerminationCharacter((UINT8)'e', 1);  //Answer should be 'OKsetname', which has one duplicate 'e'

         SerialDriver_TransmitPacket(ConfigPacketSetName, CONFIG_PACKET_SET_NAME__SIZE);

         ConfigModuleState   = SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__AWAITING_DEVICE_NAME_ACK;
         ConfigModuleTimeout = 0;
      }

      ConfigModuleTimeout++;
      if (ConfigModuleTimeout >= CONFIG_MODULE_TIMEOUT_IN_10_MS_UNITS)
      {
         DEBUGGER_BREAKPOINT(); //No response (to bluetooth config message) from bluetooth module

         SerialDriver_SetRxPacketStartCharacters(DEFAULT_COMMAND_START_CHARACTER, DEFAULT_QUERY_START_CHARACTER);
         SerialDriver_SetRxPacketTerminationCharacter(DEFAULT_TERMINATION_CHARACTER, 0);
         ConfigModuleState = SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__IDLE;
      }
   }
   else if (SerialDriver_IsTxIdle())
   {  //The serial driver transmitter is idle
      //Look for an outgoing packet buffer that is not empty. If found, send it out!
      UINT8 i;
      for (i=0; i < NUM_OUTGOING_PACKET_BUFFERS; i++)
      {
          if (OutgoingPacketBufferScanIndex >= NUM_OUTGOING_PACKET_BUFFERS)
          {
             OutgoingPacketBufferScanIndex = 0;
          }

          if (OutgoingPacketBuffersSize[OutgoingPacketBufferScanIndex] > 0)
          {
             //We found something to transmit!
             SerialDriver_TransmitPacket(OutgoingPacketBuffers[OutgoingPacketBufferScanIndex]     ,
                                         OutgoingPacketBuffersSize[OutgoingPacketBufferScanIndex]   );

             OutgoingPacketBuffersSize[OutgoingPacketBufferScanIndex] = 0;
             OutgoingPacketBufferScanIndex++;
             break;
          }
          else
          {
             OutgoingPacketBufferScanIndex++;
          }
      }
   }
}

//Look for a free outgoing packet buffer. If found, return a pointer to it. If not found, return NULL.
UINT8 * SerialBluetooth_RequestOutgoingPacketBuffer(void)
{
   UINT8 i;
   UINT8 index = OutgoingPacketBufferScanIndex;

   //Loop through outgoing packet buffers looking for an empty one
   for (i=0; i < NUM_OUTGOING_PACKET_BUFFERS; i++)
   {
      if (index >= NUM_OUTGOING_PACKET_BUFFERS)
      {
         index = 0;
      }

      if (OutgoingPacketBuffersSize[index] == 0)
      {
         //empty buffer found! Mark it and return a reference
         OutgoingPacketBufferLastReserved = index;
         return OutgoingPacketBuffers[index];
      }
      else
      {
         index++;
      }
   }

   DEBUGGER_BREAKPOINT(); //Out of TX Buffers
   return NULL; //No buffer is available.. all are used!
}

//Buffer the last reserved outgoing packet buffer as ready for transmit
void SerialBluetooth_MarkLastRequestedBufferForTransmit(UINT8 packetSize)
{
   if ( (OutgoingPacketBufferLastReserved < NUM_OUTGOING_PACKET_BUFFERS)   &&
        (OutgoingPacketBuffersSize[OutgoingPacketBufferLastReserved] == 0)    )
   {
      OutgoingPacketBuffersSize[OutgoingPacketBufferLastReserved] = packetSize;
      OutgoingPacketBufferLastReserved = INVALID_OUTGOING_PACKET_BUFFER;
   }
}

//Triggers a process whereby a name and pass key are sent to the bluetooth module
void SerialBluetooth_ConfigureModule(void)
{
   if (ConfigModuleState == SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__IDLE)
   {
      ConfigModuleState = SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__STARTING_TO_CONFIGURE;
      ConfigModuleTimeout = 0;
   }
}

static void serialBluetooth_handleDeviceNameAck(UINT8 * rxPacketPnt, UINT8 rxPacketSize)
{
   if ( (rxPacketSize < 2) || (rxPacketPnt[0] != 'O') || (rxPacketPnt[1] != 'K') )
   {
      DEBUGGER_BREAKPOINT(); //Configuration failed at 'device name' step
   }

   SerialDriver_SetRxPacketTerminationCharacter((UINT8)'N', 0); //Expected response is OKsetPIN

   SerialDriver_TransmitPacket(ConfigPacketSetPassKey, CONFIG_PACKET_SET_PASS_KEY__SIZE);

   ConfigModuleState = SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__AWAITING_PASS_KEY_ACK;
   ConfigModuleTimeout = 0;
}

static void serialBluetooth_handlePassKeyAck(UINT8 * rxPacketPnt, UINT8 rxPacketSize)
{
   if ( (rxPacketSize < 2) || (rxPacketPnt[0] != 'O') || (rxPacketPnt[1] != 'K') )
   {
      DEBUGGER_BREAKPOINT(); //Configuration failed at 'pass key' step
   }

   SerialDriver_SetRxPacketStartCharacters(DEFAULT_COMMAND_START_CHARACTER, DEFAULT_QUERY_START_CHARACTER);
   SerialDriver_SetRxPacketTerminationCharacter(DEFAULT_TERMINATION_CHARACTER, 0);

   ConfigModuleState = SERIAL_BLUETOOTH_CONFIGURE_MODULE_STATE__IDLE;
}

