/*
*------------------------------------------------------------------------------
*   Include Files
*------------------------------------------------------------------------------
*/

#include "stm8l15x.h"
#include "stdtypes.h"
#include "scheduler.h"
#include "utility.h"
#include "discover_board.h"
#include "serial_2_bluetooth.h"
#include "adc.h"

/*
*------------------------------------------------------------------------------
*   Private Defines
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Private Macros
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Private Data Types
*------------------------------------------------------------------------------
*/

struct TASK_STRUCT
{
   volatile UINT8   taskCounterValue; //DO NOT CHANGE ORDER OR TYPE. DO NOT INSERT MEMBERS.
            UINT8   taskResetValue;   //DO NOT CHANGE ORDER OR TYPE. DO NOT INSERT MEMBERS.
   volatile UINT8   isEnabled;        //DO NOT CHANGE ORDER OR TYPE. DO NOT INSERT MEMBERS.
};
typedef struct TASK_STRUCT TASK_STRUCT;

/*
*------------------------------------------------------------------------------
*   Private Function Prototypes (static)
*------------------------------------------------------------------------------
*/

static void ledHeartbeatTask(void);

/*
*------------------------------------------------------------------------------
*   Public Variables
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Private Variables (static)
*------------------------------------------------------------------------------
*/
static void (*const TaskList[MAX_TASKS])(void) =
{
   SerialBluetooth_PacketReceivedTask,
   SerialBluetooth_TxManagerTask,
   Adc_ConversionTask,
   ledHeartbeatTask
};

static TASK_STRUCT Tasks[MAX_TASKS];

/*
*------------------------------------------------------------------------------
*   Public Constants
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Private Constants (static)
*------------------------------------------------------------------------------
*/

/*
*==============================================================================
*  Functions
*==============================================================================
*/

/*
*==============================================================================
*  InitializeScheduler()
*
*  Summary:    This function intializes all memory used by the scheduler.
*              All tasks are initialized to disabled, and periodic tasks are
*              intialized to never run.
*
*  Input:      None
*
*  Output:     None
*
*  Notes:      None
*
*==============================================================================
*/
void InitializeScheduler(void)
{
   MemClear(Tasks, sizeof(Tasks));

   //Initialize Periodic Tasks:
   //-------------------------
   //                        Period                Task                               StartTime (from now)
   SetPeriodicTaskParameters(TASK_10MSEC_PERIOD  , SERIAL_BLUETOOTH_TX_MANAGER_TASK , TASK_2MSEC_PERIOD) ;
   SetPeriodicTaskParameters(TASK_4MSEC_PERIOD   , ADC_CONVERSION_TASK              , TASK_4MSEC_PERIOD) ;
   SetPeriodicTaskParameters(TASK_50MSEC_PERIOD  , LED_HEARTBEAT_TASK               , TASK_6MSEC_PERIOD) ;

}

/*
*==============================================================================
*  RunScheduler()
*
*  Summary:    This function determines the highest priority task waiting to
*              run and calls that task to run.  This function is to be placed
*              in the forever running loop in the "main()" routine.
*
*  Input:      None
*
*  Output:     None
*
*  Notes:      None
*
*==============================================================================
*/
void RunScheduler(void)
{
   UINT8 task;

   // Check the task table for an enabled task
   for (task=0; task < (UINT8)MAX_TASKS; task++)
   {
      if (Tasks[task].isEnabled)
      {
         // We found a task to run!
         Tasks[task].isEnabled = FALSE;

         // Run the current task.
         TaskList[task]();
      }
   }

   // No Tasks were run...
}


/*
*==============================================================================
*  EnableTask()
*
*  Summary:    This function is used to enable a defined task to run as soon as
*              possible.
*
*  Input:      TASK_ENUM TaskId
*
*  Output:     None
*
*  Notes:      Should *NOT* be called from within an interrupt!
*
*==============================================================================
*/
void EnableTask(TASK_ENUM TaskId)
{
   Tasks[TaskId].isEnabled = TRUE;
}


/*
*==============================================================================
*  DisableTask()
*
*  Summary:    This function is used to disable a defined task if not already
*              disabled.
*
*  Input:      TASK_ENUM TaskId
*
*  Output:     None
*
*  Notes:      Should *NOT* be called from within an interrupt!
*
*==============================================================================
*/
void DisableTask(TASK_ENUM TaskId)
{
   Tasks[TaskId].isEnabled = FALSE;
}


/*
*==============================================================================
*  EnableTaskFromInterrupt()
*
*  Summary:    This function is used to enable a defined task to run as soon as
*              possible.
*
*  Input:      TASK_ENUM TaskId
*
*  Output:     None
*
*  Notes:      *Should only* be called from within an interrupt!
*
*==============================================================================
*/
void EnableTaskFromInterrupt(TASK_ENUM TaskId)
{
   Tasks[TaskId].isEnabled = TRUE;
}


/*
*==============================================================================
*  DisableTaskFromInterrupt()
*
*  Summary:    This function is used to disable a defined task if not already
*              disabled.
*
*  Input:      TASK_ENUM TaskId
*
*  Output:     None
*
*  Notes:      *Should only* be called from within an interrupt!
*
*==============================================================================
*/
void DisableTaskFromInterrupt(TASK_ENUM TaskId)
{
   Tasks[TaskId].isEnabled = FALSE;
}


/*
*==============================================================================
*  SetPeriodicTaskParameters()
*
*  Summary:    This function initializes the timer and timer reset value for a
*              defined periodic task.  Note, this function is capable of
*              creating a one-shot task (runs only once after its timer
*              expires) by setting "TimerResetValue" to 0.
*
*  Input:      UINT8     TimerResetValue
*              TASK_ENUM TaskId
*              UINT8     TimerValue
*
*  Output:     None
*
*  Notes:      This function cannot be called from an IRQ because the
*              checkPeriodicTaskCountersTask is modifying these values not
*              within a critical section (for speed reasons).
*
*==============================================================================
*/
void SetPeriodicTaskParameters(UINT8 TimerResetValue, TASK_ENUM TaskId, UINT8 TimerValue)
{
   BEGIN_CRITICAL_SECTION();
   Tasks[TaskId].taskCounterValue = TimerValue;
   Tasks[TaskId].taskResetValue   = TimerResetValue;
   END_CRITICAL_SECTION();
}


/*
*==============================================================================
*  ClearPeriodicTaskParameters()
*
*  Summary:    This function resets the timer and timer reset value for a
*              defined periodic task.  It also disables the task if it is
*              currently enabled to run.  Note, this function should NOT be
*              called from an ISR because the protected "DisableTask()"
*              routine is called, which disables and re-enables IRQs.  (This
*              may allow one IRQ to then iterrupt another already in progress.)
*
*  Input:      TASK_ENUM TaskId
*
*  Output:     None
*
*  Notes:      This function cannot be called from an IRQ because the
*              checkPeriodicTaskCountersTask is modifying these values not
*              within a critical section (for speed reasons).
*
*==============================================================================
*/
void ClearPeriodicTaskParameters(TASK_ENUM TaskId)
{
   BEGIN_CRITICAL_SECTION();
   Tasks[TaskId].taskCounterValue = 0;
   Tasks[TaskId].taskResetValue = 0;
   Tasks[TaskId].isEnabled = FALSE;
   END_CRITICAL_SECTION();
}

/*
*==============================================================================
*  SchedulerTimerInterruptHandler()
*
*  Summary:    The scheduler tick is using the System Real Time Interrupt
*              feature of the S08.
*
*  Input:      None
*
*  Output:     None
*
*  Notes:      Called every 2.048ms
*
*==============================================================================
*/
void SchedulerTimerInterruptHandler(void)
{
   volatile UINT8* taskCounterVal;
   volatile UINT8* taskResetVal;
   volatile UINT8* maxTaskPointer;

   //Check periodic task counters:
   //********************* DANGER ZONE! *********************
   // WARNING: THE CODE INSIDE THIS DANGER ZONE IS UGLY AND UNINTUITIVE. IT
   //          IS OPTIMIZED PURELY FOR SPEED. PLEASE EXERCISE EXTREME
   //          CAUTION IF YOU'RE GOING TO CHANGE ANYTHING IN THIS DANGER
   //          ZONE! AND PLEASE DONT CODE LIKE THIS UNLESS YOU HAVE TO!
   taskCounterVal = (&(Tasks[0].taskCounterValue));
   //lint -e{416}   (Likely access/creation of out-of-bounds pointer)
   maxTaskPointer = taskCounterVal + ( sizeof(TASK_STRUCT) * (UINT8)(MAX_TASKS) );
   //lint -e{661,662}   (Possible access/creation of out-of-bounds pointer)
   while (taskCounterVal < maxTaskPointer)
   {
      if ( (*taskCounterVal) > 0 )
      {
         (*taskCounterVal)--; //decrement taskCounterValue
         if ((*taskCounterVal) == 0) //if taskCounterValue has reached 0
         {
            taskResetVal = taskCounterVal + 1; //taskResetValue is the next member in the struct after taskCounterValue
            *taskCounterVal = *taskResetVal; //set taskCounterValue to taskResetValue

            taskResetVal++; //isEnabled is the next member in the struct after taskCounterValue
            *taskResetVal = TRUE; //enable task
         }
      }
      else
      {
         taskResetVal = taskCounterVal + 1; //taskResetValue is the next member in the struct after taskCounterValue
         *taskCounterVal = *taskResetVal; //set taskCounterValue to taskResetValue
      }

      taskCounterVal += 3; //point to next task counter val (there are 3 bytes in the struct)
   }
   //******************* END DANGER ZONE! *******************
}

UINT8 CommandReceivedCountdown = 0;
void NotifyLedEngineCommandReceived(void)
{
   CommandReceivedCountdown = 10; //500ms = 10 * 50ms
}

static void ledHeartbeatTask(void)
{
   static UINT8 alternateCounter = 0;

   if (CommandReceivedCountdown > 0)
   {
      if (CommandReceivedCountdown & 0x01)
      {
         GPIO_HIGH(LED_GREEN_PORT, LED_GREEN_PIN);
         GPIO_HIGH(LED_BLUE_PORT,  LED_BLUE_PIN);
      }
      else
      {
         GPIO_LOW(LED_GREEN_PORT, LED_GREEN_PIN);
         GPIO_LOW(LED_BLUE_PORT,  LED_BLUE_PIN);
      }

      CommandReceivedCountdown--;
      if (CommandReceivedCountdown == 0)
      {
         GPIO_HIGH(LED_GREEN_PORT, LED_GREEN_PIN);
         GPIO_LOW(LED_BLUE_PORT,   LED_BLUE_PIN);
         alternateCounter = 0;
      }
   }
   else
   {
      if (alternateCounter >= 10) //10 is 500ms
      {
         GPIO_TOGGLE(LED_GREEN_PORT, LED_GREEN_PIN);
         GPIO_TOGGLE(LED_BLUE_PORT,  LED_BLUE_PIN);
         alternateCounter = 0;
      }
      alternateCounter++;
   }
}

/*
*  End of $RCSfile: scheduler.c $
*/
