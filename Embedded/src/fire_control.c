#include "stm8l15x.h"
#include "stdtypes.h"
#include "serial_3_commands.h"
#include "utility.h"
#include "fire_control.h"
#include "timer.h"

enum
{
   FIRE_CONTROL_READINESS_STATE__COCKED_AND_READY_TO_FIRE = 0,
   FIRE_CONTROL_READINESS_STATE__SLACKING                 = 1,
   FIRE_CONTROL_READINESS_STATE__COCKING                  = 2,
   FIRE_CONTROL_READINESS_STATE__IDLE_NOT_COCKED          = 3,
   FIRE_CONTROL_READINESS_STATE__REVERSING_FOR_AUTO_LATCH = 4,
   FIRE_CONTROL_READINESS_STATE__FIRING                   = 5,

   FIRE_CONTROL_READINESS_STATE__UNKNOWN               = 0xFF
};
static UINT8 FireControlReadinessState;

enum
{
   COCK_STATE__COCKED_AND_SLACKED   = 0,
   COCK_STATE__COCKED_BUT_SLACKING  = 1,
   COCK_STATE__COCKING              = 2,
   COCK_STATE__NOT_COCKED_AND_IDLE  = 3,

   COCK_STATE__UNKNOWN              = 0xFF
};
static UINT8 CockState;

enum
{
   FIRE_STATE__IDLE                          = 0,
   FIRE_STATE__REVERSING_TO_ALLOW_AUTO_LATCH = 1,
   FIRE_STATE__FIRING                        = 2,

   FIRE_STATE__UNKNOWN                       = 0xFF
};
static UINT8 FireState;

#define DEFAULT_DUTY_CYCLE_FOR_COCKING        (2000) //2000/20000 = 10%
//static UINT16 DutyCycleForCocking;
#define DEFAULT_ADC_VOLTAGE_FOR_COCKING_STOP (0x04D9) //1V
//static UINT16 AdcVoltageForCockStop;
#define DEFAULT_DUTY_CYCLE_FOR_SLACKING       (1000) //1000/20000 = 5%
static UINT16 DutyCycleForSlacking;
#define DEFAULT_NUM_PULSES_FOR_SLACK_STOP       (60)
static UINT16 NumPulsesForSlackStop;

#define DEFAULT_DUTY_CYCLE_FOR_FIRING         (2200) //2200/20000 = 11%
//static UINT16 DutyCycleForFiring;
#define DEFAULT_NUM_PULSES_FOR_FIRING           (10)
//static UINT16 NumPulsesForFiring;
#define DEFAULT_DUTY_CYCLE_FOR_REVERSING       (800) // 800/20000 = 4%
static UINT16 DutyCycleForReversing;
#define DEFAULT_NUM_PULSES_FOR_REVERSING       (100)
static UINT16 NumPulsesForReversing;

void FireControl_Init(void)
{
   FireControlReadinessState = FIRE_CONTROL_READINESS_STATE__UNKNOWN;
   CockState                 = COCK_STATE__UNKNOWN;
   FireState                 = FIRE_STATE__UNKNOWN;

   //DutyCycleForCocking       = DEFAULT_DUTY_CYCLE_FOR_COCKING;
   //AdcVoltageForCockStop     = DEFAULT_ADC_VOLTAGE_FOR_COCKING_STOP;
   DutyCycleForSlacking      = DEFAULT_DUTY_CYCLE_FOR_SLACKING;
   NumPulsesForSlackStop     = DEFAULT_NUM_PULSES_FOR_SLACK_STOP;

   //DutyCycleForFiring        = DEFAULT_DUTY_CYCLE_FOR_FIRING;
   //NumPulsesForFiring        = DEFAULT_NUM_PULSES_FOR_FIRING;
   DutyCycleForReversing     = DEFAULT_DUTY_CYCLE_FOR_REVERSING;
   NumPulsesForReversing     = DEFAULT_NUM_PULSES_FOR_REVERSING;
}

void FireControl_CockLatchAndSlack( UINT16 dutyCycleForCocking   ,
                                    UINT16 adcVoltageForCockStop ,
                                    UINT16 dutyCycleForSlacking  ,
                                    UINT16 numPulsesForSlackStop   )
{
   //DutyCycleForCocking   = dutyCycleForCocking;
   //AdcVoltageForCockStop = adcVoltageForCockStop;

   DutyCycleForSlacking  = dutyCycleForSlacking;
   NumPulsesForSlackStop = numPulsesForSlackStop;

   if (dutyCycleForCocking != 0xFFFF)
   {
      CockState                 = COCK_STATE__COCKING;
      FireControlReadinessState = FIRE_CONTROL_READINESS_STATE__COCKING;

      Timers_SetCockSlackPwm(dutyCycleForCocking, 0x0000); //0x0000 pulses means forever
      //RDN TODO give adcVoltageForCockStop to ADC module
   }
   else //if (dutyCycleForCocking == 0xFFFF)
   {
      CockState                 = COCK_STATE__COCKED_BUT_SLACKING;
      FireControlReadinessState = FIRE_CONTROL_READINESS_STATE__SLACKING;

      Timers_SetCockSlackPwm(dutyCycleForSlacking, numPulsesForSlackStop);
   }

   SerialCommands_TransmitCockResponse(CockState);
}

void FireControl_CockingSenseVoltageReached(void)
{
   if (CockState == COCK_STATE__COCKING)
   {
      CockState                 = COCK_STATE__COCKED_BUT_SLACKING;
      FireControlReadinessState = FIRE_CONTROL_READINESS_STATE__SLACKING;

      Timers_SetCockSlackPwm(DutyCycleForSlacking, NumPulsesForSlackStop);

      SerialCommands_TransmitCockResponse(CockState);
   }
   else
   {
      DEBUGGER_BREAKPOINT(); //wtf?
   }
}

void FireControl_CockingCountdownExpired(void)
{
   if (CockState == COCK_STATE__COCKED_BUT_SLACKING)
   {
      CockState                 = COCK_STATE__COCKED_AND_SLACKED;
      FireControlReadinessState = FIRE_CONTROL_READINESS_STATE__COCKED_AND_READY_TO_FIRE;

      SerialCommands_TransmitCockResponse(CockState);
   }
   else
   {
      DEBUGGER_BREAKPOINT(); //wtf?
   }
}

UINT8 FireControl_GetCockState(void)
{
   return CockState;
}

void FireControl_FireAndReverse( UINT16 dutyCycleForFiring     ,
                                 UINT16 numPulsesForFiring     ,
                                 UINT16 dutyCycleForReversing  ,
                                 UINT16 numPulsesForReversing   )
{
   //DutyCycleForFiring        = dutyCycleForFiring;
   //NumPulsesForFiring        = numPulsesForFiring;
   DutyCycleForReversing     = dutyCycleForReversing;
   NumPulsesForReversing     = numPulsesForReversing;

   FireState                 = FIRE_STATE__FIRING;
   FireControlReadinessState = FIRE_CONTROL_READINESS_STATE__FIRING;
   CockState                 = COCK_STATE__NOT_COCKED_AND_IDLE;

   Timers_SetFireReversePwm(dutyCycleForFiring, numPulsesForFiring);
}

void FireControl_FireCountdownExpired(void)
{
   if (FireState == FIRE_STATE__FIRING)
   {
      FireState                 = FIRE_STATE__REVERSING_TO_ALLOW_AUTO_LATCH;
      FireControlReadinessState = FIRE_CONTROL_READINESS_STATE__REVERSING_FOR_AUTO_LATCH;

      Timers_SetFireReversePwm(DutyCycleForReversing, NumPulsesForReversing);
   }
   else //if (FireState == FIRE_STATE__REVERSING_TO_ALLOW_AUTO_LATCH)
   {
      FireState                 = FIRE_STATE__IDLE;
      FireControlReadinessState = FIRE_CONTROL_READINESS_STATE__IDLE_NOT_COCKED;
   }

   SerialCommands_TransmitFireResponse(FireState);
}

UINT8 FireControl_GetFireState(void)
{
   return FireState;
}

UINT8 FireControl_GetReadinessState(void)
{
   return FireControlReadinessState;
}
