#include "stm8l15x.h"
#include "stdtypes.h"
#include "serial_3_commands.h"
#include "utility.h"
#include "timer.h"
#include "yaw.h"
#include "pitch.h"
#include "fire_control.h"

#define TIM2_DEFAULT_PRESCALER TIM2_Prescaler_2 //Divides 2MHz sys clock to 1MHz
#define TIM2_DEFAULT_MODULUS   20000

#define TIM3_DEFAULT_PRESCALER TIM3_Prescaler_2 //Divides 2MHz sys clock to 1MHz
#define TIM3_DEFAULT_MODULUS   20000

static UINT16 YawPulseCountdown   = 0;
static UINT16 CockPulseCountdown  = 0;
static UINT16 PitchPulseCountdown = 0;
static UINT16 FirePulseCountdown  = 0;

//Initialize the TIM2 (PWM for yaw and cock/slack), TIM3 (PWM for pitch and fire/reverse)
//and TIM4 (scheduler tick) timers
void Timers_Init(void)
{
   //Initialize TIM2 (used for yaw, and cock/slack). Default prescaler is 2, which makes
   //the tick time 1us. Default modulo is 20,000, which makes the timer wrap around at
   //20ms. Set an interrupt to happen at wrap around. This will call Timers_Tim2PulseCompleteInterrupt().
   CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);
   TIM2_TimeBaseInit(TIM2_DEFAULT_PRESCALER, TIM2_CounterMode_Up, TIM2_DEFAULT_MODULUS);
   TIM2->IER = TIM_IER_UIE; //Wraparound interrupt enable
      TIM2->CCMR1 = 0x60; //Channel 1 (Yaw) on 'output PWM1' mode
      TIM2->CCMR2 = 0x60; //Channel 2 (Cock/Slack) on 'output PWM1' mode
      TIM2->CCER1 = 0x11; //Enable output to pin on channel 1 (yaw) and channel 2 (cock/slack)
   TIM2_Cmd(ENABLE);

   //Initialize TIM3 (used for pitch, and fire/reverse). Default prescaler is 2, which makes
   //the tick time 1us. Default modulo is 20,000, which makes the timer wrap around at
   //20ms. Set an interrupt to happen at wrap around. This will call Timers_Tim3PulseCompleteInterrupt().
   CLK_PeripheralClockConfig(CLK_Peripheral_TIM3, ENABLE);
   TIM3_TimeBaseInit(TIM3_DEFAULT_PRESCALER, TIM3_CounterMode_Up, TIM3_DEFAULT_MODULUS);
   TIM3->IER = TIM_IER_UIE; //Wraparound interrupt enable
      TIM3->CCMR1 = 0x60; //Channel 1 (Pitch) on 'output PWM1' mode
      TIM3->CCMR2 = 0x60; //Channel 2 (Fire/Reverse) on 'output PWM1' mode
      TIM3->CCER1 = 0x11; //Enable output to pin on channel 1 (pitch) and channel 2 (fire/reverse)
   TIM3_Cmd(ENABLE);

   //Initialize the scheduler timer. HSI is 2MHz, and we want the scheduler interrupt
   //every 2ms (500Hz). If we set the prescaler to 16 we clock this timer at 125kHz.
   //To get from 125kHz to 500Hz, we divide by 250 (=> set auto reload to 250)
   CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);
   TIM4_TimeBaseInit(TIM4_Prescaler_16, 250);
   TIM4->IER = TIM4_IER_UIE; //Interrupt enable
   TIM4_Cmd(ENABLE);
}

void Timers_SetTim2Prescaler(UINT8 newPrescaler)
{
   TIM2->PSCR = (newPrescaler & 0x07);
}
void Timers_SetTim2ToDefaultPrescaler(void)
{
   TIM2->PSCR = ((UINT8)TIM2_DEFAULT_PRESCALER & 0x07);
}
void Timers_SetTim2Modulus(UINT16 newModulus)
{
   TIM2->ARRL = (UINT8)(newModulus >> 0);
   TIM2->ARRH = (UINT8)(newModulus >> 8);
}
void Timers_SetTim2ToDefaultModulus(void)
{
   TIM2->ARRL = (UINT8)(TIM2_DEFAULT_MODULUS >> 0);
   TIM2->ARRH = (UINT8)(TIM2_DEFAULT_MODULUS >> 8);
}
UINT8 Timers_GetTim2Prescaler(void)
{
   return TIM2->PSCR;
}
UINT16 Timers_GetTim2Modulus(void)
{
   UINT16 modulus;
   modulus  = ((UINT16)(TIM2->ARRL)) << 0;
   modulus |= ((UINT16)(TIM2->ARRH)) << 8;
   return modulus;
}

void Timers_SetYawPwm(UINT16 compareVal, UINT16 numPulses)
{
   //Yaw motor is on channel 1 of timer 2.
   TIM2->CCR1L = (UINT8)(compareVal & 0xFF);
   TIM2->CCR1H = (UINT8)((compareVal >> 8) & 0xFF);
   YawPulseCountdown = numPulses;
}

void Timers_SetCockSlackPwm(UINT16 compareVal, UINT16 numPulses)
{
   //Yaw motor is on channel 2 of timer 2.
   TIM2->CCR2L = (UINT8)(compareVal & 0xFF);
   TIM2->CCR2H = (UINT8)((compareVal >> 8) & 0xFF);
   CockPulseCountdown = numPulses;
}

void Timers_Tim2PulseCompleteInterrupt(void)
{
   if ( (YawPulseCountdown > 0) && (YawPulseCountdown != 0xFFFF) )
   {
      YawPulseCountdown--;
      if (YawPulseCountdown == 0)
      {
         //Yaw motor is on channel 1 of timer 2. Set its CCR to 0x0000 to disable it
         TIM2->CCR1L = 0x00;
         TIM2->CCR1H = 0x00;

         Yaw_YawCountdownExpired();
      }
   }

   if ( (CockPulseCountdown > 0) && (CockPulseCountdown != 0xFFFF) )
   {
      CockPulseCountdown--;
      if (CockPulseCountdown == 0)
      {
         //Cocking motor is on channel 2 of timer 2. Set its CCR to 0x0000 to disable it
         TIM2->CCR2L = 0x00;
         TIM2->CCR2H = 0x00;

         FireControl_CockingCountdownExpired();
      }
   }
}

void Timers_SetTim3Prescaler(UINT8 newPrescaler)
{
   TIM3->PSCR = (newPrescaler & 0x07);
}
void Timers_SetTim3ToDefaultPrescaler(void)
{
   TIM3->PSCR = ((UINT8)TIM3_DEFAULT_PRESCALER & 0x07);
}
void Timers_SetTim3Modulus(UINT16 newModulus)
{
   TIM3->ARRL = (UINT8)(newModulus >> 0);
   TIM3->ARRH = (UINT8)(newModulus >> 8);
}
void Timers_SetTim3ToDefaultModulus(void)
{
   TIM3->ARRL = (UINT8)(TIM3_DEFAULT_MODULUS >> 0);
   TIM3->ARRH = (UINT8)(TIM3_DEFAULT_MODULUS >> 8);
}
UINT8 Timers_GetTim3Prescaler(void)
{
   return TIM3->PSCR;
}
UINT16 Timers_GetTim3Modulus(void)
{
   UINT16 modulus;
   modulus  = ((UINT16)(TIM3->ARRL)) << 0;
   modulus |= ((UINT16)(TIM3->ARRH)) << 8;
   return modulus;
}

void Timers_SetPitchPwm(UINT16 compareVal, UINT16 numPulses)
{
   //Yaw motor is on channel 1 of timer 3.
   TIM3->CCR1L = (UINT8)(compareVal & 0xFF);
   TIM3->CCR1H = (UINT8)((compareVal >> 8) & 0xFF);
   PitchPulseCountdown = numPulses;
}

void Timers_SetFireReversePwm(UINT16 compareVal, UINT16 numPulses)
{
   //Yaw motor is on channel 2 of timer 3.
   TIM3->CCR2L = (UINT8)(compareVal & 0xFF);
   TIM3->CCR2H = (UINT8)((compareVal >> 8) & 0xFF);
   FirePulseCountdown = numPulses;
}

void Timers_Tim3PulseCompleteInterrupt(void)
{
   if ( (PitchPulseCountdown > 0) && (PitchPulseCountdown != 0xFFFF) )
   {
      PitchPulseCountdown--;
      if (PitchPulseCountdown == 0)
      {
         //Pitch motor is on channel 1 of timer 3. Set its CCR to 0x0000 to disable it
         TIM3->CCR1L = 0x00;
         TIM3->CCR1H = 0x00;

         Pitch_PitchCountdownExpired();
      }
   }

   if ( (FirePulseCountdown > 0) && (FirePulseCountdown != 0xFFFF) )
   {
      FirePulseCountdown--;
      if (FirePulseCountdown == 0)
      {
         //Fire/Reverse motor is on channel 2 of timer 3. Set its CCR to 0x0000 to disable it
         TIM3->CCR2L = 0x00;
         TIM3->CCR2H = 0x00;

         FireControl_FireCountdownExpired();
      }
   }
}
