#ifndef _YAW_H
#define _YAW_H

extern void    Yaw_Init(void);
extern void    Yaw_ExecuteMove( INT16 angle, UINT16 dutyCycle, UINT16 numPulses);
extern void    Yaw_YawCountdownExpired(void);
extern INT16   Yaw_GetYawAngle(void);
extern BOOLEAN Yaw_GetYawState(void);

#endif