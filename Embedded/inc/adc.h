#ifndef _ADC_H
#define _ADC_H

extern void Adc_Init(void);
extern void Adc_ConversionTask(void);
extern void Adc_GetCurrentAdcVoltages(UINT16 * pitch, UINT16 * pitchStop, UINT16 * plungerStop);

#endif