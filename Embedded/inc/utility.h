#ifndef UTILITY_H
#define UTILITY_H

/*
*------------------------------------------------------------------------------
*   Include Files
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Public Defines
*------------------------------------------------------------------------------
*/
#define COMPARE_EQUAL      (0)
#define COMPARE_LESS       (1)
#define COMPARE_GREATER    (-1)

/*
*------------------------------------------------------------------------------
*   Public Macros
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Public Data Types
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Public Variables (extern)
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Public Constants (extern)
*------------------------------------------------------------------------------
*/

extern const UINT8 BIT8_ARRAY[8];
extern const UINT16 BIT16_ARRAY[16];
extern const UINT32 BIT32_ARRAY[32];

/*
*------------------------------------------------------------------------------
*   Public Function Prototypes (extern)
*------------------------------------------------------------------------------
*/

extern void MemCopy(void *Dst, const void *Src, UINT8 Length);
extern void MemClear(void *Dst, UINT8 Length);
extern void MemSet(void *Dst, UINT8 Length, UINT8 Value);
extern INT8 MemCompare(const void *Src1, const void *Src2, UINT8 Length);
extern BOOLEAN MemCompareEqual(const void *Src1, const void *Src2, UINT8 Length);
extern BOOLEAN MemCompareLessThan(const void *Src1, const void *Src2, UINT8 Length);
extern UINT16 Mult8BitPercent(UINT16 value, UINT8 percent);

#endif
/*
*  End of $RCSfile: utility.h $
*/
