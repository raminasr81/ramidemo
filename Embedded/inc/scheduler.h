#ifndef SCHEDULER_H
#define SCHEDULER_H

/*
*------------------------------------------------------------------------------
*   Include Files
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Public Defines
*------------------------------------------------------------------------------
*/

enum TASK_ENUM // new type for scheduler tasks
{
   SERIAL_BLUETOOTH_PACKET_RECEIVED_TASK = 0,
   SERIAL_BLUETOOTH_TX_MANAGER_TASK      = 1,
   ADC_CONVERSION_TASK                   = 2,
   LED_HEARTBEAT_TASK                    = 3,
   /***** MUST BE LAST *****/
   MAX_TASKS,
   INVALID_TASK = 0xFF,
   /***** MUST BE LAST *****/
};
typedef enum TASK_ENUM TASK_ENUM;

// Scheduler timer tick value.  (Time (in ms) between scheduler timer IRQs.)
#define SCHED_TICK_VAL              (2)

// Various task periods for use throughout application code.
#define TASK_MSEC_PERIOD(t)         ( t / SCHED_TICK_VAL)
#define TASK_2MSEC_PERIOD           ( 2 / SCHED_TICK_VAL)
#define TASK_4MSEC_PERIOD           ( 4 / SCHED_TICK_VAL)
#define TASK_6MSEC_PERIOD           ( 6 / SCHED_TICK_VAL)
#define TASK_8MSEC_PERIOD           ( 8 / SCHED_TICK_VAL)
#define TASK_10MSEC_PERIOD          (10 / SCHED_TICK_VAL)
#define TASK_12MSEC_PERIOD          (12 / SCHED_TICK_VAL)
#define TASK_14MSEC_PERIOD          (14 / SCHED_TICK_VAL)
#define TASK_16MSEC_PERIOD          (16 / SCHED_TICK_VAL)
#define TASK_18MSEC_PERIOD          (18 / SCHED_TICK_VAL)
#define TASK_20MSEC_PERIOD          (20 / SCHED_TICK_VAL)
#define TASK_30MSEC_PERIOD          (30 / SCHED_TICK_VAL)
#define TASK_50MSEC_PERIOD          (50 / SCHED_TICK_VAL)
#define TASK_100MSEC_PERIOD        (100 / SCHED_TICK_VAL)
#define TASK_500MSEC_PERIOD        (500 / SCHED_TICK_VAL)

/*
*------------------------------------------------------------------------------
*   Public Macros
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Public Data Types
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Public Variables (extern)
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Public Constants (extern)
*------------------------------------------------------------------------------
*/

/*
*------------------------------------------------------------------------------
*   Public Function Prototypes (extern)
*------------------------------------------------------------------------------
*/

extern void InitializeScheduler(void);
extern void RunScheduler(void);
extern void EnableTask(TASK_ENUM TaskId);
extern void DisableTask(TASK_ENUM TaskId);
extern void EnableTaskFromInterrupt(TASK_ENUM TaskId);
extern void DisableTaskFromInterrupt(TASK_ENUM TaskId);

extern void SetPeriodicTaskParameters(UINT8 TimerResetValue, TASK_ENUM TaskId, UINT8 TimerValue);
extern void ClearPeriodicTaskParameters(TASK_ENUM TaskId);

extern void SchedulerTimerInterruptHandler(void);

extern void NotifyLedEngineCommandReceived(void);

#endif
/*
*  End of $RCSfile: scheduler.h $
*/
