#ifndef _SERIAL_3_COMMANDS_H
#define _SERIAL_3_COMMANDS_H



extern void SerialCommands_HandleReceivedCommand(UINT8 * commandBuffer, UINT8 commandSize);
extern void SerialCommands_TransmitYawResponse(UINT16 angle, UINT8 yawState);
extern void SerialCommands_TransmitPitchResponse(UINT16 angle, UINT8 pitchState);
extern void SerialCommands_TransmitCockResponse(UINT8 cockState);
extern void SerialCommands_TransmitFireResponse(UINT8 fireState);

#endif
