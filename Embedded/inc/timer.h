#ifndef _TIMER_H
#define _TIMER_H

extern void   Timers_Init(void);

extern void   Timers_SetTim2Prescaler(UINT8 newPrescaler);
extern void   Timers_SetTim2ToDefaultPrescaler(void);
extern void   Timers_SetTim2Modulus(UINT16 newModulus);
extern void   Timers_SetTim2ToDefaultModulus(void);
extern UINT8  Timers_GetTim2Prescaler(void);
extern UINT16 Timers_GetTim2Modulus(void);

extern void   Timers_SetYawPwm(UINT16 compareVal, UINT16 numPulses);
extern void   Timers_SetCockSlackPwm(UINT16 compareVal, UINT16 numPulses);
extern void   Timers_Tim2PulseCompleteInterrupt(void);

extern void   Timers_SetTim3Prescaler(UINT8 newPrescaler);
extern void   Timers_SetTim3ToDefaultPrescaler(void);
extern void   Timers_SetTim3Modulus(UINT16 newModulus);
extern void   Timers_SetTim3ToDefaultModulus(void);
extern UINT8  Timers_GetTim3Prescaler(void);
extern UINT16 Timers_GetTim3Modulus(void);

extern void   Timers_SetPitchPwm(UINT16 compareVal, UINT16 numPulses);
extern void   Timers_SetFireReversePwm(UINT16 compareVal, UINT16 numPulses);
extern void   Timers_Tim3PulseCompleteInterrupt(void);

#endif