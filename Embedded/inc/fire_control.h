#ifndef _FIRE_CONTROL_H
#define _FIRE_CONTROL_H

extern void  FireControl_Init(void);

extern void  FireControl_CockLatchAndSlack( UINT16 dutyCycleForCocking   ,
                                            UINT16 adcVoltageForCockStop ,
                                            UINT16 dutyCycleForSlacking  ,
                                            UINT16 numPulsesForSlackStop   );
extern void  FireControl_CockingSenseVoltageReached(void);
extern void  FireControl_CockingCountdownExpired(void);
extern UINT8 FireControl_GetCockState(void);

extern void  FireControl_FireAndReverse( UINT16 dutyCycleForFiring     ,
                                         UINT16 numPulsesForFiring     ,
                                         UINT16 dutyCycleForReversing  ,
                                         UINT16 numPulsesForReversing   );
extern void  FireControl_FireCountdownExpired(void);
extern UINT8 FireControl_GetFireState(void);

extern UINT8 FireControl_GetReadinessState(void);

#endif
