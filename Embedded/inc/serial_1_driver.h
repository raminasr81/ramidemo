#ifndef _SERIAL_1_DRIVER_H
#define _SERIAL_1_DRIVER_H

#define MAX_SERIAL_PACKET_SIZE               100
#define NO_PACKET_START_CHARACTER_EXPECTED (0xFF)
#define DEFAULT_COMMAND_START_CHARACTER    ('#')
#define DEFAULT_QUERY_START_CHARACTER      ('?')
#define DEFAULT_RESPONSE_START_CHARACTER   ('~')
#define DEFAULT_TERMINATION_CHARACTER      ('\n')

extern void    SerialDriver_Init(void);
extern void    SerialDriver_RxInterrupt(void);
extern void    SerialDriver_SetRxPacketStartCharacters(UINT8 newRxPacketCommandStartCharacter,
                                                       UINT8 newRxPacketQueryStartCharacter   );
extern void    SerialDriver_SetRxPacketTerminationCharacter(UINT8 newRxPacketTerminationCharacter, UINT8 duplicatesExpected);
extern UINT8 * SerialDriver_GetRxPacket(UINT8 * packetSize);
extern void    SerialDriver_NotifyDoneProcessingPacket(void);
extern void    SerialDriver_TxInterrupt(void);
extern BOOLEAN SerialDriver_IsTxIdle(void);
extern void    SerialDriver_TransmitPacket(const UINT8 * packetToTransmit, UINT8 packetSize);

#endif
