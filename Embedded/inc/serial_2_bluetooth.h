#ifndef _SERIAL_2_BLUETOOTH_H
#define _SERIAL_2_BLUETOOTH_H


extern void    SerialBluetooth_Init(void);
extern void    SerialBluetooth_PacketReceivedTask(void);
extern void    SerialBluetooth_TxManagerTask(void);
extern UINT8 * SerialBluetooth_RequestOutgoingPacketBuffer(void);
extern void    SerialBluetooth_MarkLastRequestedBufferForTransmit(UINT8 packetSize);
extern void    SerialBluetooth_ConfigureModule(void);

#endif
