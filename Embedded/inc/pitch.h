#ifndef _PITCH_H
#define _PITCH_H


extern void Pitch_Init(void);
extern void Pitch_ExecuteMove( INT16 angle, UINT16 dutyCycle, UINT16 adcVoltage, UINT16 numPulses);
extern void Pitch_PitchCountdownExpired(void);
extern INT16 Pitch_GetPitchAngle(void);
extern BOOLEAN Pitch_GetPitchState(void);

#endif
