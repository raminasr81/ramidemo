﻿Design documents for things that exist at the interfaces between disciplines. The interfaces are:

- PC to Embedded: The bluetooth protocol, the command set of data that is transferred back and forth
- Embedded to Analog: The interface between the micro and any actuator hardware (in this project there are no
sensors in hardware, so its one way from the micro out.. for example PWM to a motor)
- Analog to Mechanical: How the electrical components are housed, and how the actuators connect to the mechanical/structural components